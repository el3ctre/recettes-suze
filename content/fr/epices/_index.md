---
title: "Epices"
pre: <i class="fa-thin fa-salt-shaker"></i>
date: 2019-01-01T01:00:00-01:00
weight: 10
author: Suze
---

**COLOMBO** : Ragoût de viande (Poulet, Cabri, Lapin, Porc)

**BOIS D’INDE** : Brochettes, ragoût, fricassée, boudin créole, grillades (toujours
écrasé)

**CANNELLE** : Vin chaud, riz au lait, crème anglaise, confiture, punch, gâteaux,
pudding, flan

**MOUTARDE** : Steak, jardinière, ratatouille, salade, blanquette, ragoût, civet

**GIROFLE** : Soupe, potage, ragoût

**SAFRAN** : Paella, soupe de poisson, riz (ne jamais faire rissoler)

**FENOUIL** : Blaff, grillades, digestion

**CURRY** : Poulet, dinde, riz (ajouter du lait de coco pour adoucir la volaille)

**FENUGREC** : court bouillon, soupe de poisson, poulet, légumes (remplace
oignons et citrons)

**PIMENT (oiseaux)** : Mettre dans l’huile, les pizzas, les grillades, les sauces

**PAPRIKA** : Pour les coulis, mayonnaise, ratatouilles, sauces

**CACAO** : Râper dans le lait, faire cuire 5 minutes à feu doux, ajouter du
sucre, faire fondre pour les gâteaux, la mousse au chocolat, les
sorbets

**CUMIN** : Fromage, pizzas, sauces, grillades
