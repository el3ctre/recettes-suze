---
date: 2023-01-13T01:00:00-01:00
title: "Fondue"
author: Justine
tags: ["savoie"]
---

# Fondue

1 verre de vin blanc /personne  
250/300g de fromage râpé pour 2  
kirsh  
1 cc de maïzena  
1 pincée bicarbonate  
1 gousse d'ail  


Frotter le poelon à l'ail  
Délayer la maïzena dans le kirsh, y ajouter le bicarbonate, y mettre dans le poelon  

Faire chauffer le vin blanc  
Ajouter le fromage au fur et à mesure tout en remuant
