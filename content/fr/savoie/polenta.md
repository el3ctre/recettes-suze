---
date: 2019-01-01T01:00:00-01:00
title: "LA POLENTA"
author: Hughette Montfort
tags: ["plats"]
---

# LA POLENTA
## par Huguette Montfort

à préparer avec une viande en sauce

Préparation/Cuisson: 15 minutes ~

Ingrédients:
- 1L de lait
- 1 morceau de beurre (~30g)
- ~175g de polenta moyenne
- fromage râpé (1 bonne poignée)
- noix de muscade
- poivre
- sel

Porter à ébullition le lait avec le beurre.  
Verser la polenta en pluie fine. Bien tourner.  
Ajouter le fromage rapé.  
Assaisonner avec muscade, poivre & sel.  
Bien brasser.  

Note: la préparer au dernier moment. 
