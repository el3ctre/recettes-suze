---
date: 2019-01-01T01:00:00-01:00
title: "GATEAU ROULE"
author: Paroles d’expert
tags: ["dessert"]
---

# GATEAU ROULE
## (recette Paroles d’expert)

Ingrédients :  
7 jaunes d’oeufs  
55 g sucre semoule  
4 blancs d’oeuf  
30 g de sucre  
35 g de farine  
40 g de fécule  

Mélanger les 7 jaunes d’oeuf avec 55 g de sucre. Fouetter jusqu’à ce que
le mélange blanchisse.  

Ajouter une pincée de sel à 4 blancs et 30 g de sucre. Battre en neige  

Mélanger 1/3 des blancs aux jaunes, puis les 2/3 restants.  

Incorporer au mélange et à travers un chinois le mélange de 35 g de
farine et de 40 g de fécule.  

Mettre un papier à cuisson sur le moule (40 x 60). Verser et étaler en une
couche de 1 cm.  

Faire cuire dans le four chauffé à 220°C pendant 6 à 7 min. Surveiller la
cuisson.  

Retourner sur un torchon. Enlever le papier en humidifiant si nécessaire.  

Mettre la garniture et rouler le gâteau.  
