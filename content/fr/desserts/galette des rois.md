---
date: 2023-01-13T01:00:00-01:00
title: "Galette des rois"
author: Justine
tags: ["dessert"]
---

# Galette des rois - à la pomme

Pour 8 personnes :  

2 pâtes feuilletées  
5 cuillères de sucre fin  
1 c.à.s de farine  
1 kg de pomme  
1⁄2 c.à.c de cannelle en poudre  
1 jaune d'oeuf délayé avec un peu de lait pour dorer  
1 fève  


Quelques heures à l'avance préparer la compote (il faut qu'elle soit froide pour l'étaler sur la pâte)

- Eplucher les trois quarts des pommes (500 g de fruits épluchés) et les couper en petits morceaux.

- Mettre dans une casserole avec 2 cuillères à soupe de sucre et la cannelle, couvrir et laisser cuire à feu doux pendant 25 mn (pour éviter que la compote ne brûle au départ, on peut ajouter une cuillère à soupe d’eau). Oter le couvercle et laisser réduire quelques minutes sur feu moyen pour évacuer l’excès de liquide, en mélangeant bien pour ne pas laisser brûler (attention ça arrive très vite).

- Laisser refroidir.


Etaler la moprmière pâte sur une feuille de papier sulphurisé

Mélanger une cuillère de sucre et une cuillère de farine et l’étaler sur le fond de pâte.

Répartir la compote de pomme au milieu en **laissant 3 / 4 cm sur les bords**, mettre la fève, et disposer le restant de pommes (qu’on aura préalablement épluché et coupé en lamelles) sur la compote.

Saupoudrer 2 cuillerées à soupe de sucre sur les pommes et fermer avec la seconde pâte feuilletée. (coller à l'eau, puis plisser le bord avec un couteau à beurre)

Dessiner sur le dessus de la pâte et **Faire une toute petite cheminée (une incision suffit)** pour laisser échapper la vapeur pendant la cuisson.

Mettre au four préchauffé à 200°C pendant 15 mn (les cinq premières minutes on pourra poser la tourtière directement sur la sole du four pour saisir la pâte du dessous, mais ensuite poser la galette sur une grille à mi-hauteur du four).

Sortir la galette en laissant le four chauffer, et dorer avec l’oeuf délayé, remettre au four pour 15 mn.

## Version à la noisette 

Pour 8 personnes :  
2 pâtes feuilletées  
3 oeufs (2 entiers + 1 blanc pour la frangipane / 1 jaune délayé avec un peu de lait pour dorer)  
1 sachet de sucre vanillé  
200g de beurre pommade  
100g de sucre  
100g de sucre glace  
200g de poudre de noisettes  

Dans un récipient, bien mélanger la poudre de noisettes, le sucre glace et le sucre vanillé.  
Travailler le beurre en pommade et l'incorporer au mélange noisette-sucre.  
Ajouter ensuite les oeufs progressivement.  

Préchauffer le four à 200°C.  
Etaler une pâte feuilletée dans un moule à tarte et la piquer avec un fourchette.  
Verser la crème à la noisette sur le fond.  
Mettre la fève, vers le bord de préférence pour avoir plus de chance de ne pas tomber sur elle en coupant la galette.  

Retrousser les bords de la pâte sur la crème et recouvrir avec la deuxième pâte feuilletée préalablement piquée avec une fourchette. 
Former les bords de la galette.  
Avec le dos d'un couteau, décorer la galette.  
Ajouter un peu d'eau au jaune d'oeuf. Le passer ensuite sur la surface de la galette avec un pinceau.  
Mettre au four pour 30 à 35 minutes.  
Laisser reposer quelques instants sur une grille pour évacuer l'humidité et rendre la pâte croustillante.  
