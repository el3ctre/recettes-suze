---
date: 2019-01-01T01:00:00-01:00
title: "ANANAS EN MILLEFEUILLE AUX CROQUANTS DE MIEL"
author: Suze
tags: ["dessert"]
---

# ANANAS EN MILLEFEUILLE AUX CROQUANTS DE MIEL

Préparation : 35 min  

Cuisson : 15 min  

Pour 4 personnes :  
1 ananas Victoria  
40 g de beurre  
50 g de sucre, 50 g de miel  
50 g de farine  
1 cuillère à soupe d’amandes effilées  
1 cuillère à soupe d’amandes hachées  
250 ml de crème liquide UHT très froide  
1 sachet de sucre vanillé  

Préchauffez le four à 180°C. Garnissez une plaque de papier spécial
cuisson.  

Réunissez le beurre, le miel et le sucre dans une petite casserole. Portez
à ébullition puis versez, en tournant, sur la farine mélangée aux amandes
effilées, dans une jatte.  

Déposez 8 petites cuillères de pâte très espacées (car elles vont s’étaler
énormément à la cuisson) sur la plaque. Cuisez 5 min au four. Sortez la plaque
du four, laissez tiédir un peu, puis décollez les croquants et mettez-les à
refroidir sur une grille. Renouvelez l’opération avec le reste de pâte.  

Pelez l’ananas, coupez-le en quartiers, puis en tranches. Fouettez la
crème et le sucre vanillé en chantilly bien ferme.  

Au moment de servir, posez 1 croquant sur chaque assiette. Couvrez
d’une cuil de Chantilly, garnissez de tranches d’ananas, et ainsi de suite en
terminant avec un croquant. Parsemez d’amandes hachées.  
