---
date: 2019-01-01T01:00:00-01:00
title: "CREME GLACEE A L’ABRICOT AUX COPEAUX DE CHOCOLAT"
author: Suze
tags: ["dessert"]
---

# CREME GLACEE A L’ABRICOT AUX COPEAUX DE CHOCOLAT

Préparation : 20 min  
Cuisson : 10 min  
Réfrigération : 3 h  

Pour 6 à 8 personnes :  
500 g abricots   
160 g sucre en poudre   
1 citron  
2 blancs d’œufs  
20 cl crème fraîche liquide UHT  
75 g copeaux de chocolat  

Lavez, ouvrez et dénoyautez les abricots. Mettez-les dans une casserole avec 80 g sucre
et 20 cl d’eau, faites-les cuire 10 min à couvert sur feu doux. Laissez tiédir.  

Passez le contenu de la casserole au mixeur avec le jus de citron, puis laissez
complètement refroidir la purée obtenue.  

Battez les blancs d’œufs en neige très ferme, puis incorporez-leur le reste de sucre en
continuant à fouetter pour les meringuer.  

Fouettez la crème fraîche bien froide en chantilly.  

Incorporez-la délicatement aux blancs d’œufs, ajoutez la purée d’abricots, puis les
copeaux de chocolat.  

Versez le mélange dans un bac à glace et mettez au congélateur pendant 2 heures
minimum.  

30 minutes avant de servir, mettez la glace au réfrigérateur.  
