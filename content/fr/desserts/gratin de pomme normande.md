---
date: 2019-01-01T01:00:00-01:00
title: "Gratin de pomme à la normande"
author: Suze
tags: ["dessert"]
---

# GRATIN DE POMME A LA NORMANDE

Il faut pour 4 personnes :  

600 g de pommes (reinettes du Mans de préférence)  
30 g de sucre  
20 ml de calvados  
beurre (pour les ramequins)  
125 ml de lait  
1 cuillère à poudre de sucre  
1/3 de gousse de vanille  
2 jaunes d’oeufs  
2 blancs d’oeufs  
1 cuillère à soupe de sucre en poudre  

Pour la garniture : 1/4 l de crème fraîche  
 1 cuillère à soupe de sucre glace  

Temps de préparation 30 à 40 min (+ 1 h environ de macération)  

Temps de cuisson : 20 min environ  

1. Epluchez les pommes, évidez-les et coupez-les en gros cubes ou en
lamelles.  
 Mettez les dans un plat, saupoudrez-les de 30 g de sucre et arrosez-les
de calvados.  
 Couvrez et laissez macérer 1 h environ.  

 2. Beurrez 4 ramequins et répartissez-y les morceaux de pommes avec leur
jus de macération.  

3. Faites bouillir le lait avec la cuillerée à soupe de sucre et la gousse de
vanille fendue dans le sens de la longueur. Retirez la casserole du feu et
enlevez la gousse de vanille ; extrayez-en la pulpe en grattant l’intérieur et
remettez-la dans le lait. Laissez refroidir. Ajoutez alors les jaunes d’oeufs l’un
après l’autre dans le lait.  
Battez les blancs en neige pas trop durs et incorporez-les au reste.
Répartissez le mélange sur les pommes.  
Faites cuire les gratins environ 10 min sur une grille à mi-hauteur du four,
préalablement chauffé à 210 °C, puis saupoudrez de sucre et poursuivez la
cuisson une dizaine de minutes.  

4. Battez la crème fraîche avec le sucre jusqu’à ce que vous obteniez un
mélange ferme, mais pas trop dur, versez sur les gratins de pommes chauds
juste avant de servir.  

