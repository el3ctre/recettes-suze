---
date: 2019-01-01T01:00:00-01:00
title: "CRUMBLE AUX FRUITS ROUGES ET AUX POMMES"
author: Suze
tags: ["dessert"]
---

# CRUMBLE AUX FRUITS ROUGES ET AUX POMMES

Préparation : 15 à 20 minutes  

Cuisson :  
fruits rouges : 15 min  
gratin : 25 à 30 min à 225 °C  

Ingrédients :  
fruits : 300 g de fruits rouges (cassis, framboises, fraises etc)  
700 g de pommes à cuire en gros cubes  
100 g de sucre  
1 cuil à soupe de jus de citron  
1/2 zeste de citron râpé  

Pâte :  
125 g de farine  
1/2 cuil à café de vanille en poudre  
1 cuil à café de levure alsacienne  
100 g de sucre  
50 g de beurre mou  

Faites cuire les pommes doucement avec eau, sucre et zeste pendant
environ 15 min ou jusqu’à ce qu’elles soient presque tendres.  

Pendant ce temps, travaillez à la fourchette, dans un bol, les ingrédients
de la pâte, pour avoir une grosse chapelure.  

Versez les fruits rouges et les pommes dans un plat à pie ou à gratin : il
doit y avoir très peu de jus sinon ôtez-en. Saupoudrez-les de cette grosse
chapelure puis faites cuire à four chaud 25 à 30 min ou jusqu’à ce que ce soit
doré.  

Servez tiède, accompagné de crème fraîche.  
