---
date: 2019-01-01T01:00:00-01:00
title: "FEUILLES D’AUTOMNE"
author: Nicole Martino
tags: ["dessert"]
---

# FEUILLES D’AUTOMNE
## (Secret de Nicole Martino)

Pour 4/5 personnes  

Pâte du croustillant :  
30 g de beurre  
sucre glace  
30 g de miel  
30 g de farine  
1 blanc d’oeuf - 1 pointe de gingembre frais râpé  

Crème caramel aux noix :  
100 g de cerneaux de noix fraîches  
10 g de sucre semoule  
300 ml de crème liquide  

1. Mélanger dans l’ordre tous les ingrédients de façon à obtenir une pâte
homogène.  
Avec une cuillère à café, poser la pâte en petits tas bien espacés, sur une
plaque Téfal beurrée. A l’aide de 2 doigts, élargir des tas avec un mouvement
de rotation pour avoir des cercles de 7 cm de diamètre environ, bien réguliers.  

Cuire dans le four à chaleur moyenne (170/180°C), dans la partie basse
du four. Faire apparaître une couleur blonde, puis les retirer aussitôt de la
plaque. Les disposer dans un endroit sec. Faire plusieurs plaques, mais bien
laisser refroidir la plaque entre chaque cuisson.  

2. Mettre un saladier à givrer dans le congélateur.  

Hacher les cerneaux de noix grossièrement. Dans une casserole faire
cuire 50 g de sucre, quand il blondit, ajouter les 50 g restants, puis quand le
caramel fonce, ajouter un peu d’eau. Ajouter les noix hachées et travailler au
feu avec une spatule de bois pour bien enrober les noix. Quand le caramel est
brun (couleur miel, sinon amertume), verser 100 ml de crème liquide pour
l’allonger. Laisser bouillir 5 min, puis débarrasser dans un récipient au frais.
Au moment de servir, mettre les 200 ml de crème froide dans le saladier
givré et monter en chantilly. Puis incorporer délicatement au caramel aux noix  

3. Monter les feuilles d’automne : un croustillant
 une couche de crème, 3 fois  

Terminer par un croustillant, saupoudrer de sucre glace.  
