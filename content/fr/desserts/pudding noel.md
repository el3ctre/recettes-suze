---
date: 2019-01-01T01:00:00-01:00
title: "PUDDING DE NOEL"
author: Mamie
tags: ["dessert"]
---

# PUDDING DE NOEL
## (Recette de Mamie)

Ingrédients :  
1 paquet de biscottes (325 g)  
1 litre 1/4 de lait tiède  
2 cuillères à soupe de sucre  
2 cuillères à soupe de sucre vanillé  
100 g de beurre fondu  
3 jaunes d’oeuf  
3 blancs montés en neige  
raisins secs (noirs et petits)  

Mélanger les biscottes, le lait chaud, le sucre pour avoir une pâte molle.  
Ajouter les jaunes d’oeuf, le beurre fondu et mixer.  
Ajouter les raisins trempés dans du rhum  
Mélanger avec les blancs battus en neige  

Mettre dans un plat à pudding beurré.  

Faire cuire lentement 2 h à 2 h 1/2 avec un four à 160°C (environ).  

Imbiber de sirop au rhum quelques jours avant de consommer.  


*transmis par tradition orale.....*  
