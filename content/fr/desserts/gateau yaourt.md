---
date: 2019-01-01T01:00:00-01:00
title: "Gateau au yaourt"
author: Suze
tags: ["dessert"]
---

# GATEAU AU YAOURT

Cuisson : environ 30 min  

Mesure : 1 pot de yaourt  

Ingrédients : 1 pot de yaourt  
2 pots de sucre  
3 pots de farine  
1/2 pot d’huile ou de beurre fondu  
3 oeufs  
1 1/2 cuillère à café de levure  

Mettre le tout dans un saladier, mélanger, mettre dans un plat.  
Ajouter les fruits choisis.  