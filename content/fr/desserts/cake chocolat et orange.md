---
date: 2019-01-01T01:00:00-01:00
title: "CAKE AU CHOCOLAT ET A L’ORANGE"
author: Suze
tags: ["dessert"]
---

# CAKE AU CHOCOLAT ET A L’ORANGE

Pour 2 cakes de 18 x 8 :  
200g de farine  
50 g de cacao  
1 sachet de levure chimique  
250 g de sucre en poudre  
5 oeufs  
125 g de raisins de Smyrne  
300 g d’écorces d’oranges confites  
Grand Marnier  

Faire tremper les raisins secs la veille.  
Faire ramollir le beurre et le battre avec le sucre jusqu’à ce que le mélange soit
mousseux. Ajouter les oeufs, la farine, le cacao et la levure préalablement tamisés
ensemble.  

Ajouter les raisins et les cubes d’écorce d’orange.  
Mettre dans un moule beurré/fariné  
Cuire 1 h au four à 250° puis abaissé à 180° quand on enfourne.  

Fendre la croûte sur toute la longueur avec un couteau beurré.  
Vérifier la cuisson, démouler.  
Arroser avec le sirop : 150 ml d’eau, 140 g de sucre, 150 ml de Grand Marnier.  
