---
date: 2019-01-01T01:00:00-01:00
title: "LE POIRAT"
author: La Chancellerie, Orléans
tags: ["dessert"]
---

# LE POIRAT
## (La Chancellerie, Orléans)

Pour 8 personnes :  

700 g de poires (conférences, williams, comices ou passe-crassane)  
60 g de sucre en poudre  
50 ml d’alcool de poire  
1 pointe de couteau de poivre  
3 cuillères à soupe de crème fraîche épaisse  
1 oeuf pour dorer le poirat  

Pour la pâte brisée :  
300 g de farine  
150 g de beurre  
sel, eau fraîche  

Préparation : 30 min Repos : 30 min Cuisson : 55 min  

1. Préparez a pâte brisée. Mettez la farine dans une terrine avec une
pincée de sel. Coupez le beurre en copeaux. Du bout des doigts, effritez-le
dans la farine. Versez peu à peu l’eau fraîche en mélangeant rapidement.  
Formez une boule en travaillant le moins possible. La pâte doit être souple,
ferme et ne pas coller. Laissez reposer 30 min au réfrigérateur.  
2. Pendant ce temps, épluchez les poires. Retirez le coeur, coupez-les en
tranches épaisses. Mettez-les dans une terrine. Arrosez avec l’alcool de poires.
Mélangez le sucre en poudre et le poivre et saupoudrez-en les poires. Laissez
macérer 30 min.  
3. Etalez la pâte brisée en un grand cercle de 1/2 cm d’épaisseur.  
Egouttez soigneusement les poires en conservant le jus. Déposez au centre de
la pâte les quartiers de poire Rabattez la pâte vers le centre afin de recouvrir le
tout. Ménagez au milieu une petite ouverture dans laquelle vous glisserez une
cheminée en carton. Dorez au pinceau la pâte avec l’oeuf battu.  
4. Faites cuire le poirat 35 min à four préchauffé à 220 °C puis 20 min
environ à 180 °C.  
5. Délayez la crème fraîche avec un peu de jus de macération pour
obtenir une crème semi-liquide. Versez-la dans le poirat par petites quantités
par la cheminée en inclinant le gâteau.  
Si vous souhaitez une présentation moins rustique, faites deux abaisses
de pâte et cuisez dans un moule à bord haut, la 2e abaisse recouvrant
totalement les poires sera pincée sur la 1ère. On peut aussi poudrer le fond de
pâte d’un mélange fait de 30 g de poudre d’amandes et de 30 g de sucre en
poudre sur le quel on posera les quartiers de poire  
