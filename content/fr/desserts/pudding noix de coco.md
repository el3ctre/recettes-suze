---
date: 2019-01-01T01:00:00-01:00
title: "PUDDING A LA NOIX DE COCO"
author: Gisèle
tags: ["dessert"]
---

# PUDDING A LA NOIX DE COCO
## (recette Gisèle)

120 g de beurre 280 g de coco râpé  
2 l de lait concentré 200 g de sucre  
240 g de farine un peu de levure  
4 oeufs  

Dans le lait, vous mettez le coco.  
Faire cuire 35 min à feu doux.  

Pendant que le lait est sur le feu, battre en crème : beurre, sucre, le jaune
d’œuf.  

Lorsque le coco est cuit et refroidi, y mêler la farine, la levure et le beurre
mélangé, puis le blanc d’oeuf battu en neige.  

Verser dans un moule et faire cuire 40 min, thermostat 7.  
