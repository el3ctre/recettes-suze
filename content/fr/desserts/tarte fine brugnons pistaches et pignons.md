---
date: 2019-01-01T01:00:00-01:00
title: "TARTE FINE AUX BRUGNONS, PISTACHES ET PIGNONS"
author: Suze
tags: ["dessert"]
---

# TARTE FINE AUX BRUGNONS, PISTACHES ET PIGNONS

Préparation : 20 min  
Cuisson : 35 min  

Pour 6 personnes :  
300 g de pâte sablée  
1 kg de brugnons  
100 g de pistaches décortiquées non salées  
100 g de pignons  
130 g de sucre glace  
1 oeuf   
50 g de crème épaisse  
1 cuil à soupe de flocons d’avoine  

Allumez le four à 210°C. Etalez la pâte dans une tourtière antiadhésive
(ou bien beurrée). Piquez le fond de tarte à la fourchette. Garnissez-le d’un
disque de papier sulfurisé et couvrez de légumes secs. Faites pré cuire le fond
de tarte 15 min, puis sortez le du four et retirez le lest.  

Mixez la moitié des pistaches et des pignons. Ajoutez 100 g de sucre
glace, l’oeuf, la crème et remettez le mixeur en marche, jusqu’à obtenir une
masse homogène. Etalez la préparation dans le fond de tarte et saupoudrez de
flocons d’avoine.  

Dénoyautez les brugnons, coupez-les en lamelles et rangez-les en rosace
sur la tarte. Parsemez de pistaches et de pignons. Faites cuire au four 20 min.  

Laissez refroidir la tarte, puis démoulez-la et saupoudrez de sucre glace
au travers une passoire fine. Servez froid.  