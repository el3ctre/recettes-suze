---
date: 2019-01-01T01:00:00-01:00
title: "Baba au Rhum"
author: Suze
tags: ["dessert"]
---

# BABA AU RHUM

Ingrédients pour 6 personnes :  
La pâte : 270 g de farine  
 10 g de levure de boulanger  
 3 cuil. de lait + 1/2 verre (100 ml)  
 2 pincées de sel  
3 oeufs  
2 cuil. à café de sucre en poudre  
80 g de beurre  

Le sirop : 3 cuil. à soupe de rhum vieux + 1 petite louche  
2 verres d’eau  
300 g de sucre en poudre  

La crème : 500 g de crème liquide  
1 pincée de sel  
4 cuil. à soupe de sucre en poudre  
vermicelle en chocolat  

> Mettre un saladier mouillé dans le freezer pour qu’il givre. Emiettez 10g de levure de
boulanger dans un verre, 1/2 h après, versez dessus 3 cuil. à soupe de lait et faites fondre
la levure. Dans un appareil électrique pétrisseur, mettez 270 g de farine, ajoutez le
mélange levure-lait, pétrissez à vitesse lente, puis cassez un oeuf dans l’appareil et
ajoutez deux pincées de sel. Pétrissez à vitesse moyenne, cassez un deuxième oeuf,
laissez tourner 1 min, puis un 3e oeuf. Passez alors à vitesse rapide pendant 5 min pour
obtenir une pâte classique. Ajoutez petit à petit 1/2 verre (100 ml) de lait tempéré, 2 cuil. à
café de sucre et sans s’arrêter de pétrir, ajoutez 80 g de beurre ramolli et laissez pétrir
encore 1 min.  

Versez cette pâte dans le saladier, recouvrez-le avec un linge et laissez reposer 15 min à
température ambiante.  

A l’aide d’une spatule de caoutchouc, brassez la pâte un peu montée pour la casser et
versez- la dans un moule à baba de 24 cm beurré. Tapez le moule sur la table pour
égaliser et tasser la pâte, puis placez-le dans un endroit tempéré pendant 45 min à 1 h. La
pâte a alors monté mais ne doit pas dépasser le bord du moule. Mettez alors au four
préchauffé à 200°C pour 15 min. Sortez le baba et démoulez-le sur la grille.  
> Versez 2 verres (400ml) d’eau et 300 g de sucre dans une casserole posée sur feu vif,
faites bouillir pour obtenir un sirop que vous laisser tiédir. Ajoutez alors 3 cuil. à soupe de
rhum vieux dans le sirop tiède et mélangez.  

> Arrosez le baba avec les 2/3 de sirop et dans ce qui reste ajoutez une petite louche de
rhum et réservez.  

> Montez la crème en chantilly dans le saladier givré avec une pincée de sel, et 2
cuil. à soupe de sucre vanillé. Ajoutez le vermicelle de chocolat. Arrosez le baba
avec le reste de sirop de rhum.
