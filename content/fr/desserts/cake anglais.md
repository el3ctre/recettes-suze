---
date: 2019-01-01T01:00:00-01:00
title: "LES SECRETS DU VRAI CAKE ANGLAIS"
author: Suze
tags: ["dessert"]
---

# LES SECRETS DU VRAI CAKE ANGLAIS

Ingrédients :  
150 g de farine  
150 g de cerises confites  
100 g de fruits confits variés  
250 g de raisins de Smyrne  
100 de sucre en poudre  
1 sachet de levure chimique  
50 g de poudre d’amandes  
100 g de beurre  
3 oeufs  
1 pincée de gingembre en poudre  
1 cuil. à café de miel  
3 cuil. à soupe de rhum  
50 g d’amandes effilées  
50 g de sucre glace  

Versez la farine dans une grande terrine. Ajoutez la moitié des cerises
confites découpées en 2 et le reste des cerises entières. Afin que ces dernières
ne tombent pas au fond du gâteau, roulez-les bien dans la farine.  

Ajoutez les fruits confits hachés. Remuez bien, toujours avec la main.  

Ajoutez ensuite les raisins, puis le sucre. Mélangez longuement.  

Ajoutez les amandes pilées, puis 100 g de beurre ramolli, en mélangeant
bien entre chaque addition  

Ajoutez les oeufs entiers, battus ensemble, et mélangez à la cuillère de
bois. Ajoutez la levure, le gingembre en poudre, puis le miel et enfin le rhum.  

Mélangez bien.  

Beurrez un grand moule à cake, versez-y la pâte, ajoutez à la surface les
amandes effilées et saupoudrez de sucre glace.  

Mettez au four à 160°C pendant 1 h environ. Le cake est à point
lorsqu’une lame de couteau plongée dedans, en ressort tout à fait sèche.  

Renversez le cake sur une grille et attendez qu’il ait tout à fait refroidi
avant de le démouler.  

Enveloppez-le de papier d’aluminium et gardez-le si possible dans un
endroit frais pendant trois jours au moins avant de l’entamer  
