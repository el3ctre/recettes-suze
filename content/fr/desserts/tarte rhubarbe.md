---
date: 2019-01-01T01:00:00-01:00
title: "TARTE A LA RHUBARBE"
author: Suze
tags: ["dessert"]
---

# TARTE A LA RHUBARBE

Pâte : 1 oeuf  
125 g sucre  
250 g de farine  
125 g de beurre  
1 pincée de sel  

Garniture :  
500 g rhubarbe  
200 g sucre  
2 Oeufs  
1 cuil. à café de cannelle  
1 cuil. à soupe de maïzena  

Préparation : 40 min  
Cuisson : 45 min  

La pâte :  
Mélanger au mixer le beurre et le sucre ; ajouter l’oeuf entier, une pincée de sel
puis la farine ; mélanger à fond entre chaque apport. Sortez la pâte, mettez-la en boules et
laisser la reposer 30 min au réfrigérateur. Etaler la au rouleau sur la table légèrement
farinée. Beurrer un moule et foncer-le avec la pâte.  

La garniture :  
Laver et éponger les tiges de rhubarbe, sans les éplucher ; détailler-les en dés.
Mélanger les oeufs, le sucre et la cannelle ; ajouter la maïzena et les dés de rhubarbe.
Verser dans le fond de la pâte.  
Dans les chutes de pâte, découper des lanières que vous disposez en croisillons sur la
garniture. Faire cuire 10 min à 210°C, puis 35 min à 150 °C.  
Laisser refroidir avant de démouler.  
Manger froid ou légèrement tiède.  