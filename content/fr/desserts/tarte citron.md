---
date: 2019-01-01T01:00:00-01:00
title: "TARTE AU CITRON"
author: Suze
tags: ["dessert"]
---

# TARTE AU CITRON

Préparation : 50 min  
Cuisson : 10 + 20 min  

Pour 8 personnes :  

Pâte sablée : 250 g de farine  
125 g de beurre  
 50 g de sucre  
1 jaune d’oeuf  
1 pincée de sel  

Garniture citron : 3 oeufs  
150 g de sucre en poudre  
2 citrons  
150 g de beurre  

Meringue italienne : 250 g de sucre  
125 g de blanc d’oeuf  
50 ml d’eau  

Préparez la pâte. Dans une terrine, mélangez oeufs, sucre et sel jusqu’à
consistance crémeuse. Ajoutez toute la farine. Effritez du bout des doigts
jusqu’à obtention d’un sable grossier. Incorporez le beurre en petits dés. Pétrir
et fraiser la pâte pour bien l’homogénéiser. Roulez la pâte en boule. La mettre
30 min au frais.  

Confectionnez la garniture. Laver soigneusement les citrons. Râper le
zeste, presser les cirons. Mélangez soigneusement les oeufs avec le sucre.
Ajoutez zeste, jus de citrons et beurre fondu. Réservez. Abaissez la pâte.
Foncez le moule. Recouvrir de papier sulfurisé et de haricots secs. Faire cuire
la pâte à blanc 10 min à 170 °C. La sortir du four. Otez papier et haricots.
Versez la préparation. Remettre au four 20 min à 220°C.  

Confectionnez la meringue. Versez dans une casserole sucre et eau.
Faire cuire à feu vif jusqu’à ce qu’une goutte de sucre versé dans l’eau froide
forme une boule qui se modèle entre les doigts. Dans un saladier montez les
blancs en neige.  

Versez lentement le sirop de sucre le long du bord du récipient, sans
cesser de fouetter jusqu’à complet refroidissement. La meringue doit devenir
épaisse et satinée. L’étaler sur la tarte au citron. La laisser dessécher 30 min
dans le four encore chaud.  
