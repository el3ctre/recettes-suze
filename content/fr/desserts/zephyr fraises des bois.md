---
date: 2019-01-01T01:00:00-01:00
title: "ZEPHYR AUX FRAISES DES BOIS"
author: Suze
tags: ["dessert"]
---

# ZEPHYR AUX FRAISES DES BOIS

Préparation : 30 min  
Réfrigération : 4 h  

Pour 6 personnes :  
250 g de fraises des bois  
200 g de mascarpone ou de crème fraîche épaisse  
1 cuil. à café de sucre glace  
150 g de sucre en poudre  
50 ml de liqueur de fraise  
1 cuil. à soupe de poudre de cacao amer  
3 oeufs, 1 pincée de sel  


Rincez rapidement et équeutez les fraises. Egouttez-les  

Séparez les blancs des jaunes d’oeufs. Fouettez les jaunes avec le sucre
en poudre jusqu’à ce que le mélange blanchisse  

Incorporez la liqueur et le mascarpone. Mélanger intimement.  
Fouettez les blancs en neige ferme avec le sel, puis incorporez-les à la
préparation.  

Ajoutez les fraises (sauf quelques unes pour la décoration). Versez dans
un compotier et réservez au réfrigérateur pendant au moins 4 h  

Au moment de servir, saupoudrez la surface de poudre de cacao à travers
une passoire fine. Roulez les fraises restantes dans le sucre glace et disposezles harmonieusement sur le dessus.  
Servez bien frais.  

