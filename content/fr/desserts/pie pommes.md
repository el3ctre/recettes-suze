---
date: 2019-01-01T01:00:00-01:00
title: "PIE AUX POMMES"
author: Suze
tags: ["dessert"]
---

# PIE AUX POMMES

Préparation : 10 min + 10 min + 10 min  
Cuisson : 30 à 40 min à 225 °C  

Ingrédients :  
1 kg de pommes à cuire (acides) épluchées et coupées en
quartiers  
100 g de sucre  
2 cuil à soupe de jus de citron  
30 g de raisins secs  

Pâte minute normale (vous n’en utiliserez que la moitié) à laquelle on
ajoute :  
1 pincée de muscade  
1 pincée de cannelle  
1 pincée de zeste de citron  

Préparez la pâte et étalez-en la moitié.  
Découpez une bande et recouvrez le bord du plat dans lequel vous
disposez :  
une couche de pommes saupoudrées de sucre, de zeste et de jus de
citron  
de tous petits dés de beurre  
des raisins ...... et recommencez jusqu’à épuisement.  

Recouvrez le tout de pâte et former un feston au bord de la pâte, dorez à
l’oeuf et faites cuire à four chaud 30 à 40 min.  

Servez tiède avec de la crème fraîche.  


*Peut se faire avec de la rhubarbe (50 g). Dans ce cas, forcez sur le sucre et remplacez le citron par de l’orange.*
