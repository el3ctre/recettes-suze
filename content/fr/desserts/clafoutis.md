---
date: 2019-01-01T01:00:00-01:00
title: "CLAFOUTIS"
author: Suze
tags: ["dessert"]
---

# CLAFOUTIS

Temps de préparation et de cuisson : 1 h  

Pour 4, il faut :  
500 g de cerises  
40 g de beurre ou de margarine  
2 cuillères à soupe pleines de farine (60 g)  
5 cuillères à soupe de sucre (125 g)  
3 oeufs  
2 verres de lait, 1 pincée de sel  

Mélangez tous les ingrédients dans une terrine en finissant par la matière
grasse. Puis les cerises non dénoyautées. Mettre dans un plat à feu beurré.  

Faire cuire 45 min environ Servir froid ou tiède.  