---
date: 2019-01-01T01:00:00-01:00
title: "COUSCOUS ALGERIEN"
author: Suze
tags: ["plats"]
---

# COUSCOUS ALGERIEN

Préparation des boulettes : hacher un oignon, de la mie de pain trempée dans le lait, un
œuf, une demie livre de viande de bœuf hachée, sel, poivre,
malaxer et faire des boulettes.  
Sauce pour le couscous : faire dorer dans un grand récipient une épaule roulée
d’agneau. La retirer. Faire revenir dans cette cocotte, un
oignon finement haché, une tomate épépinée, quelques
carottes, navets, fonds d’artichauts, petits pois à la
convenance.  
Si l’on met des courgettes, il faut les dorer à part car elles sont très fragiles et
risquent de se mettre en purée.  
Quand les légumes sont revenus, saler et poivrer. A ce moment, rajouter le jus de
cuisson des boulettes que l’on aura préalablement dégraissé.
Remettre l’épaule et faire mijoter une heure et demi à petit
feu. Ajouter un quart d’heure avant de servir, boulettes et
courgettes.  
On peut également ajouter des pois chiches qui s’achètent préalablement blanchis
ou que l’on aura fait cuire la veille.  
Pour servir, dégager boulettes et courgettes et couper l’épaule en tranches.
Il est préférable de préparer sa sauce la veille du repas pour pouvoir dégraisser
facilement après un séjour d’une nuit au réfrigérateur pour les
foies sensibles.  
Préparation de la graine : Acheter du couscous dit moyen. Le laver au robinet dans un
chinois. Le faire égoutter 5 minutes pour qu’il gonfle.  
Le verser dans un grand récipient plat. Bien l’aérer et casser les boules en les
écrasant entre les paumes. Le mouiller avec de l’eau salée.  
Le faire cuire TROIS fois, un quart d’heure chaque fois à la
vapeur.  
Entre chaque cuisson, le verser dans le grand récipient plat, le mouiller avec de
l’eau salée, l’aérer. Avant la dernière cuisson, ajouter une
cuillère à soupe d’huile par kg de graine, et bien la répartir.  
Ceci évitera aux grains de s’agglomérer.  
ET BON APPETIT………..  