---
date: 2019-01-01T01:00:00-01:00
title: "FRICASSEE D’OIE DU PAYS D’AUGE"
author: Suze
tags: ["plats"]
---

# FRICASSEE D’OIE DU PAYS D’AUGE

Préparation : 20 min  
Cuisson : 1 h 45  
Pour 6 personnes :  
1 oie de 3 kg coupée en morceaux  
3 échalotes, 1 oignon  
2 pommes vertes  
2 branches de céleri  
80 g de raisins secs  
50 ml de calvados  
300 ml de cidre brut  
3 cuil à soupe de farine  
2 cuil à café de fond de volaille déshydraté  
3 feuilles de sauge  
1 petite branche de romarin  
sel, poivre  

Réhydratez les raisins secs dans un bol d’eau tiède. Pelez et épépinez les
pommes. Coupez-les en morceaux, faites-les cuire 10 min à couvert dans une
casserole avec deux cuil d’eau. Ecrasez-les à la fourchette  
Faites chauffer la graisse d’oie dans une cocotte, puis faites-y revenir les
morceaux d’oie sur tous les côtés. Retirez-les et remplacez-les par l’oignon, les
échalotes et le céleri très finement émincés. Laisser blondir 5 min, puis
égouttez-les.  
Jetez le gras contenu dans la cocotte et remettez-y les morceaux d’oie et
la fondue échalotes. Saupoudrez de farine et remuez pendant quelques
minutes.  
Chauffez le calvados dans une casserole. Enflammez-le hors du feu et
versez-le dans la cocotte. Ajoutez le cidre, la compote de pommes, les raisins
égouttés, le fond de volaille, la feuille de sauge et le romarin. Salez, poivrez et
mélangez. Couvrez la cocotte et laissez mijoter à feu doux pendant 1 h 30.  
Déposez les morceaux d’oie dans un plat de service préalablement
chauffé. Versez la sauce par dessus et décorez éventuellement de quartiers de
pommes crues et de feuilles de céleri.  
Servez immédiatement avec par exemple, un panaché de petites pommes
de terre et de pommes fruits sautées.  