---
date: 2019-01-01T01:00:00-01:00
title: "FILET DE BOEUF EN CROUTE AU MADERE"
author: Suze
tags: ["plats"]
---

# FILET DE BOEUF EN CROUTE AU MADERE

Préparation : 1 h  
Cuisson : 40 min  

Pour 10 personnes :  
1,200 kg de filet de boeuf  
300 g de pâte feuilletée  
150 g de filet de dinde ou de noix de veau  
2 oeufs  
50 g de beurre  
150 g de crème fraîche  
2 échalotes  
1 citron  
2 cuillerées à soupe d’huile  
100 g de champignons de Paris  
sel, poivre, muscade  

Pour les crêpes :  
1 cuillerée bombée de farine  
1 oeuf  
10 cl de lait  
1 cuillerée à soupe de fines herbes hachées  

Pour la sauce :   
100 ml de madère  
200 ml de fond de veau ou de sauce demi-glace  
1 petite boîte de truffe  
50 g de beurre,  
ou 500 ml de sauce Périgueux ou Madère  

1. Parez et assaisonnez le filet de boeuf. Faites chauffer l’huile dans une
poêle sur feu vif, ajoutez le filet de boeuf, laissez-le dorer puis retournez-le pour
qu’il soit bien saisi de tous les côtés pendant 10 à 12 min. Egouttez-le et
laissez-le refroidir. Jetez l’huile, déglacez la poêle avec le Madère, laissez-le
réduire d’un tiers, ajoutez le fond de veau ou la sauce demi-glace, portez à
ébullition et passez au chinois. Réservez au chaud. Mixez le filet de dinde ou la
noix de veau. Laissez la chair dans le bol du mixer et mettez-la 15 minutes au
congélateur. Préparez la pâte à crêpes, ajoutez-y les fines herbes.  
2. Epluchez et hachez fin les échalotes et les champignons. Faites revenir
les échalotes dans une casserole avec 20 g de beurre. Lorsqu’elles sont
translucides, ajoutez les champignons, le jus de citron, du sel et du poivre.
Laissez cuire jusqu’à évaporation de l’eau de végétation. Réservez au frais.  

3. Ajoutez un blanc d’oeuf dans un bol du mixer. Faites tourner 10
secondes, versez la moitié de la crème fraîche, mixez 10 secondes et
incorporez l’autre moitié de la crème fraîche en mixant encore 10 s. Retirez la
mousse du mixer. Ajoutez le sel et le poivre, un peu de muscade et les
champignons. Mettez au frais. Confectionnez 6 crêpes aux fines herbes.  
4. Préchauffez le four th.8-225 °C. Abaissez au rouleau la pâte feuilletée
sur 2 mm d’épaisseur pour obtenir un rectangle légèrement plus long que le filet
de boeuf et suffisamment large pour l’envelopper. Battez l’oeuf entier avec le
jaune et badigeonnez en la pâte feuilletée avec un pinceau.  
Avec une spatule, enduisez le filet de boeuf de mousse aux champignons.
Entourez-le avec les crêpes et posez-le sur la pâte feuilletée. Repliez la pâte
pour bien enfermer le tout et soudez les bords. Badigeonnez d’oeuf battu et
enfournez. Cuisez à four chaud pendant 15 à 20 min.  
5. Terminez la sauce en incorporant la truffe émincée et son jus puis le
beurre en petits morceaux, en remuant à feu doux, avec la cuillère en bois.
Rectifiez l’assaisonnement. Servez le rôti avec des petits légumes ou des pâtes
fraîches.  