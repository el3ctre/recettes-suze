---
date: 2019-01-01T01:00:00-01:00
title: "MEDAILLONS DE VEAU AUX CHAMPIGNONS"
author: Suze
tags: ["plats"]
---

# MEDAILLONS DE VEAU AUX CHAMPIGNONS

Préparation : 10 minutes  
Durée de cuisson : 6 min  
Pour 4 personnes :  
8 médaillons de veau de 2 cm d’épaisseur, pris dans le filet  
250 g de champignons de Paris, 2 échalotes  
60 ml de Noilly, 200 ml de crème fleurette  
100 g de beurre, huile, noix muscade, sel et poivre  

La veille, saler et poivrer les médaillons de veau, les badigeonner d’huile
et les laisser reposer au frais.  
Faire réduire les échalotes émincées dans le Noilly, ajouter 200 ml de
crème fleurette, du poivre et une pincée de muscade puis les champignons
émincés revenus au beurre.  
Poêler les médaillons 2 min de chaque côté, les mettre ans la sauce et les
laisser mijoter 2 min.  
Les servir accompagnées de pâtes fraîches au beurre.  