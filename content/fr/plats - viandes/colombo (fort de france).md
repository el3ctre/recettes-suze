---
date: 2019-01-01T01:00:00-01:00
title: "RECETTE DE COLOMBO"
author: marché de Fort de France
tags: ["plats"]
---

# RECETTE DE COLOMBO
## donnée au marché de Fort de France

> Couper la viande en morceaux, ajouter sel, ail, oignon émincé, citron, poivre,
2 cuillères à soupe de poudre de Colombo,  
> Laissez macérer une heure au réfrigérateur (cette préparation peut être faite
la veille)  
> Faire chauffer la marmite avec de l’huile, ajouter une cuillerée de graines à
roussir (cumin, fenugrec, moutarde, les trois mélangées).  
> Faire revenir sans brûler  
> Verser tout le mélange, remuer avec une spatule pour bien saisir la viande,
mettre de l’eau au ras de la viande  
> Pendant la cuisson, ajouter les aubergines coupées en morceau et un
bouquet garni avec persil, thym (en fin de cuisson),  
> Pour terminer, mettre un kub (sic), une cuillerée à soupe de Colombo, un jus
de citron, trois gousses d’ail, puis laissez mijoter 5 minutes feu doux.  
Servez avec du riz blanc  
BON APPETIT  
Et à bientôt pour nous dire si vous vous êtes régalés.  