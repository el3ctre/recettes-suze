---
date: 2019-01-01T01:00:00-01:00
title: "CURRY D’AGNEAU"
author: Suze
tags: ["plats"]
---

# CURRY D’AGNEAU

Ingrédients :  
1 épaule d’agneau désossée et coupée en gros cubes  
500 g de collier d’agneau  
3 pommes golden  
2 bananes  
2 cuil à soupe de curry  
50 g de raisin de corinthe  
2 gros oignons  
3 tomates, 1/2 mangue  
1/2 ananas, 1 poivron rouge  
40g de farine  
50 ml d’huile d’olive  
100 ml de vin blanc sec  
4 gousses d’ail, 2 feuilles de basilic  
sel, poivre  

Coupez les tomates en 4. Epluchez les pommes, 1.5 banane et l’ail.  
Epluchez et émincez les oignons. Préchauffez le four à 240 °C. Mettez à
tremper les raisins de Corinthe dans un bol d’eau froide. Epluchez 1/4 de
mangue et 1/4 d’ananas. Faites rissoler la viande dans une sauteuse avec 50
ml d’huile d’olive en lui donnant une belle coloration. Dégraissez.  
Ajoutez l’oignon émincé, remuez, saupoudrez avec la farine et le curry.  
Remuez pour enrober la viande. Déglacez au vin blanc, salez, poivrez, ajoutez
l’ail et l’eau jusqu’à hauteur des morceaux de viande. Ajoutez les morceaux de
fruits préparés et faites cuire 1h30 à feu doux.  
Pendant de temps faites griller les poivrons au four à 240 °C pendant 10
min. A la sortie du four, épluchez-les et coupez-les en fines lanières.  
Coupez le reste de la mangue en lamelles fines, le reste d’ananas en
petits éventails, la demi banane en rondelles.  
Retirez la viande à l’aide d’une écumoire. Mixez la sauce, vérifiez
l’assaisonnement.  
Remettez la viande à chauffer dans sa sauce, ajoutez la garniture de fruits
frais et les raisins égouttés  
Servez bien chaud et décorez avec du basilic coupé très fin.  