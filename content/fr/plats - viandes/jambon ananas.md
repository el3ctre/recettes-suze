---
date: 2019-01-01T01:00:00-01:00
title: "JAMBON A L’ANANAS"
author: Suze
tags: ["plats"]
---

# JAMBON A L’ANANAS

Préparation : 15 min.  
Cuisson : 2 h + 3/4 h  
Ingrédients pour 20 personnes environ :  
1 jambon demi-sel de 3 kg  
1 gros bouquet garni, sel, poivre  
2 boites de 10 tranches d’ananas en boites ou 2 ananas frais  
20 ml de moutarde environ  
20 ml de sucre environ  

Si le jambon est demi-sel, le faire dessaler à l’eau froide une nuit.  
Faites cuire dans un grand pot au feu juste recouvert d’eau froide, avec un
bouquet garni, sel et poivre. Portez à l’ébullition, baissez et laissez frémir en
comptant 40 min par kg soit 2 h.  
Egouttez-le bien, ôtez la couenne et si nécessaire un peu de gras et
quadrillez la surface avec la pointe d’un couteau. Mêlez moutarde et sucre,
tartinez le jambon de ce mélange. Piquez de clous de girofle à l’intersection des
croisillons et faites cuire à four moyen (175°C) en comptant 1/4 h par kg pour
que le jambon soit bien doré.  
10 min avant la fin de la cuisson, placez les tranches d’ananas dans le
plat de cuisson pour les faire chauffer.  
Découpez une partie du jambon et servez-le chaud entouré de tranches
d’ananas.  
Peut être servi avec des épinards à la crème.  