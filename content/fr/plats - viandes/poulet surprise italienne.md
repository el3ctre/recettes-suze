---
date: 2019-01-01T01:00:00-01:00
title: "POULET SURPRISE A L’ITALIENNE"
author: Suze
tags: ["plats"]
---

# POULET SURPRISE A L’ITALIENNE

Pour 4 personnes :  
Préparation : 30 min  
4 blancs de poulet sans peau Cuisson : 30 min   
2 boules de mozzarella  
500 g tomates fermes  
12 tomates cocktail  
2 aubergines  
6 échalotes  
2 gousses d’ail  
8 brins de romarin  
6 c. à s. huile olive  
3 c. à s. vinaigre balsamique  
Sel et poivre  

Eliminez le pédoncule des aubergines. Taillez-les dans la longueur en au moins 8 bandes
assez fines. Salez et laissez dégorger 30 min.  
Epongez-les. Etalez-les dans un plat à gratin. Arrosez-les de 3 cuil. huile et passez-les
15 min sous le gril du four préchauffé, en les retournant à mi-cuisson.  
Pelez et émincez l’ail et les échalotes. Concassez les tomates fermes. Faites blondir les
échalotes dans 1 c. huile à la poêle. Sur feu très chaud, ajoutez l’ail et les tomates. Salez,
cuisez 3 min et arrosez de vinaigre.  
Ebouillantez les tomates cocktail. Pelez-les en laissant la peau attachée au pédoncule.
Dorez 5 min dans 1 cuil. huile les blancs de poulet salés et poivrés.  
Ouvrez-les en deux et glissez-y les aubergines, des lamelles de mozzarella et un brin de
romarin. Refermez et ficelez.  
Faites gratiner 10 min au four dans un plat huilé. Servez avec les tomates aux échalotes
et les tomates cocktail.  