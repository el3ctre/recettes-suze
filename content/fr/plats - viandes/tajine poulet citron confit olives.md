---
date: 2019-01-01T01:00:00-01:00
title: "TAJINE DE POULET AUX CITRONS CONFITS ET AUX OLIVES"
author: Suze
tags: ["plats"]
---

# TAJINE DE POULET AUX CITRONS CONFITS ET AUX OLIVES

Pour 6 personnes  
Préparation : 30 min  
cuisson : 1 h  

Ingrédients :  
1 gros poulet coupé en morceaux  
2 cirons confits au sel  
24 olives vertes blanchies  
2 oignons  
5 gousses d’ail  
50 ml d’huile  
2 cuillerées à café de grains de coriandre concassés  
2 pincées de safran en poudre  
sel, poivre  

1. Mélangez trois pincées de sel, deux pincées de poivre, la coriandre, le
safran et cuillerée d’huile.  
Enduisez le poulet de cette pâte et laissez macérer pendant 30 min.
2. Faites chauffer l’huile restante dans une cocotte. Mettez-y le poulet à
dorer sur chaque face 10 min, puis ajoutez les oignons et l’ail ciselés. Remuez
pendant 5 min.  
3. Mouillez d’eau à mi-hauteur du poulet et ajoutez les citrons en quartiers
et les olives. Laissez mijoter 35 min à couvert, retirez le poulet et sa garniture ;
réservez au chaud.  
4. Laissez réduire le fond de cuisson à découvert jusqu’à ce qu’il soit
onctueux. Remettez les morceaux de poulet dans la sauce, sur feu doux, pour
les réchauffer quelques minutes.  
5. Dressez-les dans un plat chaud et nappez de la sauce. Servez très
chaud.  