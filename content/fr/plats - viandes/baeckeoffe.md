---
date: 2019-01-01T01:00:00-01:00
title: "LE BAECKEOFFE"
author: Suze
tags: ["plats"]
---

# LE BAECKEOFFE

Pour 5 à 6 personnes :  
500 g d’échine de porc ou d’épaule de porc  
500 g d’épaule de mouton sans os  
500 g de poitrine de boeuf désossée ou de paleron   
1 kg de pommes de terre  
250 g d’oignons  
2 à 3 gousses d’ail  
1/2 l de Pinot blanc ou de Riesling  
bouquet garni, persil, thym, laurier, sel, poivre  

Détailler la viande en morceaux comme pour une estouffade et la mettre à
mariner pendant 24 h avec un peu de vin, quelques oignons, l’ail, le bouquet
garni, sel et poivre.  
Dans une cocotte en terre, disposer une couche de pommes de terre
émincées, ensuite les viandes, les oignons émincés. Mouiller avec le vin blanc
Fermer la terrine avec le couvercle et faire cuire au du boulanger pendant
2 h à 2 h 1/2.  
Servir tel quel dans la terrine dans laquelle s’est effectuée la cuisson.
Suivant le goût on peut ajouter, en plus de la viande indiquée, une queue
et un pied de porc.  
Ce met est généralement accompagné d’une salade.  