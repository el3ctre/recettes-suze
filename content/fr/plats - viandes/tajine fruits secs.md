---
date: 2019-01-01T01:00:00-01:00
title: "TAJINE AUX FRUITS SECS"
author: Suze
tags: ["plats"]
---

# TAJINE AUX FRUITS SECS

Pour 6 personnes  
Préparation 25 min  
Cuisson : 1 h 10  

Ingrédients :  
1 épaule d’agneau d’ 1.2 kg  
1 poivron rouge  
1 oignon  
2 gousses d’ail  
1 branche de céleri  
150 g de pruneaux dénoyautés  
150 g d’abricots secs  
50 g de pignons  
50 g de citron confit  
1 bouquet garni  
3 cuill à soupe d’huile  
1 cuill à soupe de miel  
1 pincée de safran  
1 cuill à soupe de graine de sésame  
1/2 cuill de cannelle  
1/2 cuill à café de cumin  
menthe, sel, poivre  

> Coupez l’agneau en morceaux de 60 g. Mettez à tremper les pruneaux
et les abricots dans l’eau tiède. Faites chauffer l’huile dans une cocotte, puis
mettez-y la viande à dorer.  
> Ajoutez l’oignon et le céleri émincés et l’ail écrasé, mélangez-les à la
viande et faire suer 5 min à feu doux. Salez et poivrez, puis ajourez le safran, le
cumin, la cannelle et le bouquet garni. Couvrez d’eau à hauteur et laissez cuire
40 min à feu doux.  
> Rincez le poivron, enlevez les graines et les cloisons blanches et
coupez-les en lanières (moi j’épluche le poivron !). Retirez le bouquet garni de
la cocotte. Egouttez les abricots et les pruneaux coupés en morceaux. Emincez
le citron confit  
> Ajoutez l’ensemble de ces ingrédients, ainsi que le miel, dans la cocotte,
mélangez et laissez cuire encore 10 min.  
> Présentez le tajine dans un plat en terre, parsemez de pignons grillés et
de graine de sésame. Décorez de quelques feuilles de menthe et servez sans
attendre.  