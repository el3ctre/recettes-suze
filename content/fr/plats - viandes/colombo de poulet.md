---
date: 2019-01-01T01:00:00-01:00
title: "COLOMBO DE POULET"
author: Suze
tags: ["plats"]
---

# COLOMBO DE POULET

Préparation : 25 min  
cuisson : 55 min  
Pour 6 personnes :  
1 beau poulet de 1.5 à 2 kg (coupé en morceaux)  
100 g de poudre à colombo  
4 citrons verts  
4 cuil d’huile  
3 gousses d’ail  
2 oignons blancs  
bouquet garni (thym, persil)  
piment  
2 feuilles de bois d’Inde ou laurier  
2 courgettes, 2 carottes  
2 aubergines, 2 tomates  
sel, poivre  

Citronnez les morceaux de poulet avec le jus des 3 citrons.  
Epluchez et émincez les oignons. Pilez les gousses d’ail.  
Dans une cocotte, faites revenir les morceaux de poulet dans le beurre et
l’huile avec l’ail et l’oignon. Salez et poivrez. Lorsqu’ils sont colorés, ajoutez la
poudre de colombo délayée dans un peu d’eau, le bouquet garni, les feuilles de
bois d’Inde, le piment épépiné et coupé en deux et laissez cuire à feu doux
pendant 45 min.  
Epluchez les carottes et les courgettes, taillez-les en rondelles. Coupez
les aubergines et les tomates en dés. Incorporez les légumes à mi-cuisson.
Au moment de servir, pilez la gousse d’ail et pressez le citron restant.  
Ajoutez au plat.  
Versez le poulet, les légumes et la sauce dans le plat de service en
enlevant le piment et les feuilles de bois d’Inde  
Servez avec du riz créole.  