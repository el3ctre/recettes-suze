---
date: 2019-01-01T01:00:00-01:00
title: "MAGRETS DE CANARD AUX EPICES"
author: Suze
tags: ["plats"]
---

# MAGRETS DE CANARD AUX EPICES

Ingrédients :  
2 magrets de canard (750 d sans peau)  
2 cuil à soupe de sauce soja  
1 cuil à soupe de poivre mignonnette  
1 cuil à café de graines de cumin  
1 cuil à café de graines de coriandre  
1 cuil à soupe de miel liquide  
1 petit céleri rave (750 g)  
10 g de beurre  
sel, poivre, persil  
1/4 de citron  

Retirer la peau des magrets et éliminez toute trace de graisse. Faites
ensuite mariner les magrets 1 h avec le soja.  
Pendant ce temps, épluchez le céleri, divisez-le en quatre et coupez les
quartiers en lamelles. Plongez celles-ci dans l’eau bouillante salée et citronnée
et faites-les cuire en les gardant un peu ferme. Puis égouttez-les et passez-les
sous l’eau froide. Préchauffez le gril du four.  
A l’aide d’un mini hachoir hachez le cumin avec la coriandre, puis
mélangez-les dans un bol avec le poivre, le miel et 1 cuillerée à café d’huile.  
Etalez la pâte obtenue sur une assiette et enduisez les magrets égouttés de
cette préparation, en formant une sorte croûte sur l’une des faces.  
Posez les magrets sur un plat à four, coté croûte d’épices vers le gril et
faites-les cuire pendant 10 à 15 minutes, en les plaçant à environ 15 cm du gril.  
Faites chauffer le beurre et 1 cuil à café d’huile dans une poêle et faites
dorer les lamelles de céleri. Salez et poivrez.  
Servez le céleri sauté avec les magrets escalopés. Parsemez de persil
haché.  
