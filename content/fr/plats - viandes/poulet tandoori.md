---
date: 2019-01-01T01:00:00-01:00
title: "POULET TANDOORI"
author: Suze
tags: ["plats"]
---

# POULET TANDOORI

Pour 4 personnes  
Préparation : 20 min  
Marinade : 24 h  
Cuisson : 45 min  

Ingrédients :  
1 poulet de 1.4 kg  
2 yaourts veloutés (ou bulgare)  
2 gousses d’ail  
6 cuil. à soupe de jus de citron vert  
2 cuil. à soupe d’huile  
30 g de beurre  
1 cuil. à café de graines de coriandre  
2 cuil. à soupe d’épices tandoori  
sel  

> La veille, coupez le poulet en morceaux, ôtez la peau, puis entaillez la
chair régulièrement.  
> Frottez-la longuement avec 2 cuil. à café de sel mélangées à 4 cuil. de
jus de citron. Mettre les morceaux de poulet dans le plat de cuisson et laissez
mariner 30 min à température ambiante.  
> Grillez les graines de coriandre à la poêle. Mixez-les en pâte avec de
l’ail, les yaourts, la poudre tandoori et 2 cuil. de jus de citron. Etalez la pâte sur
le poulet. Couvrez avec un film étirable. Laissez 24 h au frais.  
> Le lendemain, préchauffez le four à 210°C. Faites fondre le beurre,
écumez-le et transvasez-le dans un bol sans y faire couler les dépôts blancs du
fond.  
> Faites cuire le poulet 10 min, puis arrosez-le de beurre clarifié, baissez
le thermostat à 180 °C et poursuivez la cuisson pendant 35 min en arrosant de
sauce plusieurs fois.  
> Pour servir, disposez le poulet sur un grand plat. Accompagnez-le de
rondelles de tomate, coco râpé, feuille de menthe, ciboulette ciselée, chutneys
de mangue et citron vert.  