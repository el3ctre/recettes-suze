---
date: 2019-01-01T01:00:00-01:00
title: "TOURTE FARCIE"
author: Suze
tags: ["plats"]
---

# TOURTE FARCIE

Préparation : 15 min  
Cuisson : 20 à 25 min  

Pour 6 personnes :  
500 g de pâte feuilletée  
500 g de cèpes frais  
300 g d’échine de porc désossée  
300 g d’épaule de veau désossée  
3 échalotes grises, 2 brins de persil haché  
3 cuil à soupe d’huile d’olive  
2 oeufs + pour la dorure  
50 g de mie de pain rassis  
3 cuil à soupe de cognac  
sel, poivre du moulin  

Nettoyer les cèpes. Les essuyer. Réserver les pieds pour une soupe.  
Tailler les chapeaux en dés (1 cm de côté). Les saisir dans une sauteuse
contenant de l’huile d’olive chaude. Baisser le feu et poursuivre la cuisson en
remuant fréquemment pour rendre l’eau ce végétation sans que les cèpes
dessèchent.  
Egouttez les cèpes, les laisser refroidir. Hacher l’échine de porc et l’épaule
de veau, les échalotes, le persil, la mie de pain. Mélanger intimement tous les
ingrédients. Incorporer le cognac et les oeufs un par un. Saler et poivrer.  
Ajouter les cèpes tièdes. Mélanger pour obtenir une farce homogène.
Réchauffer le four à 180 °C. Partager la pâte feuilletée en 2 pâtons (un
légèrement plus grand). Les étaler (3 à 4 mm d’épaisseur). Déposer le plus
grand des disques de pâte sur la plaque recouverte de papier sulfurisé.  
Disposer la farce au centre en laissant une bande de pâte de 2 cm tout
autour. Mouiller du bout du doigt cette bande avec de l’eau. Poser le second
disque sur la farce. Souder les bords en pinçant fortement entre le pouce et
l’index. Sur le dessus dessiner un motif de la pointe du couteau. Dorer toute la
surface au pinceau avec de l’oeuf battu. Faire cuire 20 à 25 min au four.