---
date: 2019-01-01T01:00:00-01:00
title: "COURONNE DE COURGETTES A L’AGNEAU"
author: Suze
tags: ["plats"]
---

# COURONNE DE COURGETTES A L’AGNEAU

Pour 6 Personnes  
Préparation : 40 min  
Cuisson :35 min  
Ingrédients :  
500 g de viande d’agneau hachée  
3 courgettes  
les feuilles d’une botte de blettes  
1 cuil. à soupe de thym + 1 de persil ciselé  
2 gousses d’ail  
60 g de parmesan râpé  
3 oeufs  
30 g de farine tamisée  
150 ml d lait  
200 ml de crème liquide  
50 ml d’huile d’olive  
15 g de beurre + 15 g de farine (pour le moule)  
sel, poivre, muscade  

-> Préchauffez le four sur thermostat 7 (210 °C). Beurrez et farinez un moule en couronne.  
-> Découpez les courgettes en rondelles. Chauffez la moitié de l’huile dans une poêle sur
feu doux et faites-y revenir les courgettes pendant 5 min. Versez ensuite dans une
passoire fine et laissez égoutter.  
-> Faites blanchir les feuilles de blettes 5 min à l’eau bouillante salée. Egouttez-les,
rafraîchissez-les et pressez-les bien entre les mains pour les essorer. Hachez-les
grossièrement. Epluchez et dégermez l’ail, puis émincez-le.  
->Faites revenir l’ail quelques secondes dans le reste d’huile. Ajoutez la viande hachée, le
thym, le persil et les feuilles de blettes. Laissez cuire 5 min sur feu vif en remuant de
temps en temps. Salez et poivrez.  
->Tapissez le fond du moule de rondelles de courgettes en les faisant se chevaucher.
Placez une couche de farce à la viande sur les courgettes.  
-> Battez les oeufs en omelette, incorporez la farine et le parmesan en versant peu à peu
le lait et la crème. Assaisonnez de sel, de poivre, de muscade. Mélangez au reste de la
farce et versez dans le moule.  
-> Faites cuire la couronne environ 35 min au four, jusqu’à ce que la surface soit bien
dorée.  