---
date: 2023-01-13T01:00:00-01:00
title: "Soupe chinoise"
author: Justine
tags: ["soupe asie"]
---

# Soupe chinoise

BOUILLON  

1 gousse d'ail  
1 oignon émincé  
60 cl de bouillon de volaille  	
1 càs de maïzena  
1/2 càs de piment (ou des gouttes de Tabasco)  
1/2 càc de gingembre en poudre  
1 càs de sauce soja  
1/2 càs de vinaigre  	
sel  
poivre  
		
LEGUMES  

1 carotte coupée en bâtonnets  
100 g de pousses de soja (ou des épinards)  
3 ou 4 champignons noirs  
125 g de nouilles chinoises  
ou ce que vous voulez comme légumes  

VIANDES

2 blancs de poulet  


Faire revenir les blancs de poulet à la poële  
Préparer le bouillon dans la casserole  
Ajouter les légumes, Recouvrer d'eau généreusement et faire cuire  
Ajouter les blancs de poulet coupés en lamelle dans la casserole et laisser mijoter quelques minutes  
