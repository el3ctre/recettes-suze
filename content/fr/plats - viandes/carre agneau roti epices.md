---
date: 2019-01-01T01:00:00-01:00
title: "CARRE D’AGNEAU ROTI AUX EPICES"
author: Suze
tags: ["plats"]
---

# CARRE D’AGNEAU ROTI AUX EPICES

Ingrédients :  
1 carré d’agneau de 8 côtes  
1/2 citron confit, 3 dattes  
1 branche de romarin  
2 feuilles de sauge, 3 feuilles de basilic  
1 cuil à café de 5 baies  
1 étoile de badiane  
1 tête d’ail, 50 ml d’huile d’olive  
sel, poivre  

Faites enlever la colonne vertébrale du carré par votre boucher.  
Conserver l’os pour réaliser le jus.  
Parer vous-même le haut des côtes  
Préchauffer le four à 240 0C.  
Préparer le hachis. Dénoyautez les dattes, effeuillez le romarin, la sauge,
le basilic. Ajoutez le citron confit, les 5 baies, la badiane et hachez le tout
grossièrement.  
Dans une sauteuse allant au four, chauffez 50 ml d’huile d’olive et faites
colorer sur le feu le carré côté peau. Salez, poivrez, ajoutez les os réservés,
retournez, puis enfournez et faites cuire 10 min.  
Ajoutez alors le hachis sur la surface du carré et prolongez la cuisson de
10 min (pur une cuisson rosée).  
Retirez le carré du four, jetez la matière grasse. Coupez la tête d’ail en
deux, mettez-la au four 5 min, le temps qu’elle se colore.  
Déglacez le récipient avec 100 ml d’eau, faites réduire de moitié et servez
ave le carré.  

Astuce : Enlever une barrette de gras de 2 cm de largeur sur toute la
longueur de carré au sommet des côtes. Grattez avec le couteau chaque os en
poussant la chair vers le bas.  
Hachurez la surface du carré à l’aide d’un couteau, uniquement 
l’épaisseur de graisse pour faciliter la cuisson.  