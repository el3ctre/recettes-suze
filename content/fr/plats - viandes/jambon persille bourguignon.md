---
date: 2019-01-01T01:00:00-01:00
title: "JAMBON PERSILLE BOURGUIGNON"
author: Suze
tags: ["plats"]
---

# JAMBON PERSILLE BOURGUIGNON

Pour 4 à 6 personnes  
Préparation : 30 min  
Cuisson : 6 h (la veille)  
1 jambonneau demi-sel  
1 palette de porc demi-sel  
500 g de lard demi-sel  
2 poireaux  
1 carotte  
1 gros bouquet de persil plat  
6 gousses d’ail  
1 bouquet garni  
600 ml de vinaigre d’alcool  
1/2 bouteille de vin blanc sec  
10 grandes feuilles de gélatine alimentaire  

1. Détachez les tiges du persil, puis ciselez les feuilles grossièrement.
Mettez-les au frais.  
2. Dans un grand faitout, mettez les trois viandes. Ajoutez la carotte pelée
et coupée en morceaux, les poireaux nettoyés et coupés en tronçons, les
gousses d’ail pelées, le bouquet garni, les tiges de persil, 500 ml de vinaigre et
le vin. Couvrez d’eau à niveau et mettez sur feu très doux.  
3. A l’ébullition, écumez, puis réglez le feu pour que le liquide frémisse à
peine. Laissez cuire pendant 6 h. Passé ce temps, égouttez les viandes, filtrez
le bouillon de cuisson. Mesurez un litre de bouillon chaud et faites dissoudre
dedans les feuilles de gélatine préalablement ramollies à l’eau froide puis
essorées.  
4. Défaites les viandes avec deux fourchettes pour détacher les fibres, en
éliminant les os.  
5. Disposez une bonne couche de persil ciselé au fond d’un grand
saladier, puis versez un peu de gelée. Ajoutez une cuillerée de vinaigre blanc.
Couvrez d’une épaisse couche de viandes en disposant les fibres dans le
même sens. Recommencez en alternant couche épaisse de persil, gelée,
vinaigre et viandes toujours dans le même sens.  
6. Posez une assiette bien plate sur la dernière couche, mettez un poids
(par exemple une boite de conserve de 1 kg) et placez au moins 12 h au
réfrigérateur.  
7. Trempez le fond du saladier quelques secondes dans de l’eau chaude,
puis démoulez le jambon persillé sur un plat rond. Découpez en tranches
épaisses dans le sens contraire des fibres de la viande. Servez avec des
cornichons, des oignons blancs au vinaigre et une salade verte.  
8. Si vous êtes prévoyante... ou patiente, dégustez le jambon deux ou
trois jours après l’avoir confectionné. Les saveurs auront le temps de se mêler,
et le résultat n’en sera que meilleur.  