---
date: 2019-01-01T01:00:00-01:00
title: "TROUSSEPINETTE"
author: Jacques
tags: ["boissons"]
---

# TROUSSEPINETTE
## (recette Jacques)

Mélanger le même jour :  
1 kg de sucre cristallisé  
5 l de vin léger  
1 l d’eau de vie  
1 cuillérée à soupe de chicorée en grains  
1 poignée pleine de pousses jeunes (30 cm) de prunelle (épine noire) (donc en mai)  
Laisser macérer pendant 10 à 12 jours.  