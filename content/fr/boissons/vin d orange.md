---
date: 2019-01-01T01:00:00-01:00
title: "VIN D’ORANGE"
author: Suze
tags: ["boissons"]
---

# VIN D’ORANGE

500 ml d’alcool  
5 l de vin rosé  
4 oranges amères  
2 oranges douces  
1 citron  
5 clous de girofle  
1 gousse de vanille  

Mélanger les ingrédients, couvrir, patienter 40 jours, couvert filtrer, mettre en bouteilles.  