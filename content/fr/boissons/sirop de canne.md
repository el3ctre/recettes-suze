---
date: 2019-01-01T01:00:00-01:00
title: "SIROP DE CANNE “MARIE GALANTE”"
author: Suze
tags: ["boissons"]
---

# SIROP DE CANNE “MARIE GALANTE”

Ingrédients :  
1 kg de sucre roux  
1 l d’eau  
2 feuilles de laurier  
1 brin de thym frais  
1 zeste d citron vert  
1 zeste d’orange  
1 bâton de cannelle  
1 gousse de vanille  
Laisser bouillir 1/4 h, baisser le feu jusqu’à ce que le niveau ait réduit d’un quart.  
Refroidir, filtrer.  