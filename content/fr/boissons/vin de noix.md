---
date: 2019-01-01T01:00:00-01:00
title: "VIN DE NOIX"
author: Suze
tags: ["boissons"]
---

# VIN DE NOIX

30 noix cueillies entre le 25 juin et début juillet  
4 bouteilles de 750 ml de vin rouge 12 à 13°  
2 d’alcool blanc (eau de vie de fruits)  
1 sachet de sucre vanillé  
Couper les noix en quatre. Les mettre dans une bonbonne avec le vin, l’alcool et 
le sucre. Fermer hermétiquement et laisser macérer 50 jours au frais 
dans l’obscurité en remuant de temps en temps. Filtrer et embouteiller.  
A conserver frais. Se conserve au moins 1 an.  