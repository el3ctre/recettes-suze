---
date: 2019-01-01T01:00:00-01:00
title: "PUNCH COCO"
author: Suze
tags: ["boissons"]
---

# PUNCH COCO

4 verres de coco Lopez  
6 verres de rhum blanc  
3 verres de lait Nestlé sucré  
Zeste de citron vert  
cannelle, muscade, vanille  
Mixez 10 secondes et servir avec de la glace pilée  
+ 1 tranche de citron vert  