---
date: 2019-01-01T01:00:00-01:00
title: "RATAFIA DE CHAMPAGNE"
author: Suze
tags: ["boissons"]
---

# RATAFIA DE CHAMPAGNE

3 kg de raisin pour avoir 2 l de jus de raisin  
1 l d’eau de vie ou marc de champagne  
Jus de fruits + alcool dans une bonbonne  
Boucher et laisser macérer jusqu’à Noël dans un endroit frais à la lumière du jour jusqu’aux gelées.  
Quand le froid est passé, filtrer, mettre en bouteilles.  