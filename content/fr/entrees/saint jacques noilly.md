---
date: 2019-01-01T01:00:00-01:00
title: "SAINT JACQUES AU NOILLLY"
author: Suze
tags: ["entrées"]
---

# SAINT JACQUES AU NOILLLY

Préparation : 25 min  
Cuisson : 20 min  
Pour 4 personnes :  
12 coquilles Saint Jacques  
6 échalotes  
60 g de beurre  
1 belle orange  
100 ml de Noilly  
1 cuillère à soupe de farine  
200 ml de crème épaisse  
2 citrons (facultatif)  
1 cuillère à soupe de persil haché  
1/2 cuillère à café de paprika  
1 pointe de Cayenne, sel, poivre  

1. Ouvrez les coquilles en glissant une lame de couteau entre deux
valves. Détachez les noix et retirez les petites poches noires. Lavez les noix
avec soin, épongez-les, coupez-les en gros dés.  
2. Faites fondre les échalotes finement hachées dans le beurre chaud, ne
les laissez pas se colorer. Ajoutez les dés de Saint-Jacques, faites-les revenir 2
à 3 min puis mouillez le jus de la moitié de l’orange et le Noilly. Assaisonnez de
sel, poivre et de paprika. Laissez cuire 5 à 6 min.  
3. Egouttez les dés de Saint- Jacques et faites réduire de 2/3 le jus de
cuisson sur feu vif.  
4. Délayez la farine avec la crème et versez dans le jus de cuisson réduit.
Mélangez bien à la spatule et laissez épaissir. Relever l’assaisonnement avec
du Cayenne et remettez les coquilles à réchauffer dans la sauce.  
5. Garnissez de cette préparation, 4 coquilles de Saint-Jacques ou en
porcelaine. Saupoudrez de persil haché et décorez dune demi rondelle
d’orange. Accompagnez éventuellement de citron et servez chaud.  