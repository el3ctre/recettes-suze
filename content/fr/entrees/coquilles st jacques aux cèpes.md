---
date: 2019-01-01T01:00:00-01:00
title: "COQUILLES SAINT-JACQUES POELEES AUX CEPES"
author: Suze
tags: ["entrées"]
---

# COQUILLES SAINT-JACQUES POELEES AUX CEPES

Pour 4 personnes  
Préparation : 15 min  
cuisson : 15 min  
16 noix de Saint-Jacques moyennes  
500 g de cèpes frais ou en bocaux   
2 échalotes grises  
2 cuill. à café d’huile d’olive  
1/2 cuil. à café de poivre vert et de baies roses mélangés  
quelques brins de persil plat  
sel fin  

1. Rincez les noix de Saint-Jacques et épongez les. Lavez les cèpes et
épongez-les ; réservez les pieds pour un autre usage. Coupez les chapeaux en
larges lamelles. Pelez les échalotes, puis émincez les en rondelles.  
2. Faites chauffer l’huile dans une grande poêle antiadhésive et faites-y
dorer les échalotes. Réservez sur une assiette. Faites sauter les champignons
dans la poêle pendant 10 min, jusqu’à ce qu’ils soient croustillants. Salez
pendant la cuisson. Réservez avec les échalotes.  
3. Toujours dans la même poêle, faites dorer les noix de Saint-Jacques, 1
min sur chaque côté. Salez un tout petit peu. Puis ajoutez les champignons et
les échalotes, en remuant pendant 30 secondes, juste le temps de les
réchauffer.  
4. Parsemez de poivre vert et de baies roses, en les écrasant entre vos
doigts, et de persil. Servez tout de suite.  