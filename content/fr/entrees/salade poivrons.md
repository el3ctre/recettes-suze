---
date: 2019-01-01T01:00:00-01:00
title: "SALADE DE POIVRONS"
author: Suze
tags: ["entrées"]
---

# SALADE DE POIVRONS

Pour 6 personnes :  
3 poivrons verts  
3 poivrons rouges  
1 douzaine d’olives noires  
huile d’olive  
vinaigre de vin rouge  
250 g de petits oignons blancs  
2 cuillères à soupe de concentré de tomate  
50 g de raisins secs  
1 brin de thym  
1 feuille de laurier  
1 citron  
sel, poivre  

Lavez les poivrons et retirer le pédoncule. Essuyez-les et coupez-les en 2.
Retirer les graines et les cloisons internes. Aplatissez les demi poivrons avec la
paume et rangez-les sur la tôle du four. Faites-les griller en surveillant, jusqu’à
ce que la peau se boursoufle.  
Retirer les poivrons grillés du four et pelez-les, puis taillez-les en lanières
pas trop fines que vous mettrez dans un saladier. Arrosez le tout d’huile d’olive
et de vinaigre et laissez reposer au frais.  
Pelez les petits oignons et hachez-les grossièrement. Mettez-les dans une
casserole avec 3 cuillères à soupes d’huile, un verre d’eau et une cuillère à
soupe de vinaigre. Ajoutez le sucre, les raisins secs et le concentré de tomate,
le thym et le laurier. Mélangez, salez et poivrez. Faites réduire doucement en
remuant de temps en temps pendant une demi heure.  
Egouttez les poivrons et mettez-les dans un plat creux. Retirer le laurier et
le thym de la sauce, versez-la sur les poivrons et mélangez.  
Garnissez d’olives et de tranches de citron. Servez à température
ambiante.  