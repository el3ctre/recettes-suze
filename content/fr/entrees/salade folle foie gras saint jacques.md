---
date: 2019-01-01T01:00:00-01:00
title: "SALADE FOLLE DE FOIE GRAS ET DE SAINT-JACQUES"
author: Suze
tags: ["entrées"]
---

# SALADE FOLLE DE FOIE GRAS ET DE SAINT-JACQUES

Pour 4 personnes  
Préparation : 20 min  
Cuisson : 5 min  
200 g de foie gras mi-cuit  
16 noix de Saint-jacques  
20 asperges vertes surgelées  
150 g de salade mélangée  
200 ml de fumet de poisson (avec une cuil. à soupe de fumet déshydraté,
type Maggi)  
1 échalote  
1/2 bouquet de cerfeuil et de persil mélangés  
3 cuil. à soupe d’huile d’olive  
2 cuil. à soupe de vinaigre balsamique  
2 cuil. à soupe de jus d’orange  
fleur de sel, sel, poivre du moulin  

Faites cuire les asperges encore surgelées 3 min dans de l’eau bouillante
salée, égouttez. Portez le fumet de poisson salé à frémissements et faites-y
pocher les noix de Saint-Jacques 2 min sans laisser bouillir. Egouttez-les et
récupérez deux cuillerées de jus de cuisson.  
Versez ce jus dans un bol avec l’échalote finement émincée. Ajoutez le jus
d’orange, le vinaigre balsamique et l’huile d’olive ; réservez au frais.
Rincez et séchez le cerfeuil et le persil, mêlez-les avec la salade nettoyée,
répartissez le tout au centre d’assiettes de service.  
Détaillez le foie gras en fines tranches et déposez-les sur les assiettes.
Salez à la fleur de sel et poivrez au moulin. Disposez les asperges d’en
éventails, ainsi que les noix de Saint-Jacques. Juste avant de servir,
émulsionnez la sauce et répartissez-la  
Présentez cette salade très raffinée avec des tranches de pain brioché
légèrement grillées  