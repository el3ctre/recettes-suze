---
date: 2019-01-01T01:00:00-01:00
title: "COQUES SAUTEES AU LAIT DE COCO"
author: Suze
tags: ["entrées"]
---

# COQUES SAUTEES AU LAIT DE COCO

Pour 4 personnes  
Préparation : 15 minute  
Cuisson : 10 minutes  
Cette recette convient également pour les palourdes ou les moules  
1 cuil. à soupe d’huile d’arachide  
1 oignon moyen grossièrement haché  
2 gousses d’ail écrasées  
1 cuil. à soupe de citronnelle fraîche finement ciselée  
2 cuil. à café de cumin en poudre  
1 cuil. à café de curcuma en poudre  
1 kg de coques préparées  
2 cuil. à soupe de jus de citron vert  
2 cuil. à café de nuoc mâm  
400 L de lait de noix de coco 
2 cuil. à café de sucre roux  
500 g de choy sum grossièrement haché  
2 cuil. à soupe de feuilles de coriandre fraîche  

1. Faites chauffer l’huile dans un wok ou une grande sauteuse ; ajoutez l’oignon, le
gingembre, l’ail, la citronnelle et les épices moulues ; remuez jusqu’à l’odeur monte dans
la cuisine.  
2. Ajoutez les coques rincées et égouttées, puis le jus de citron vert, le nuoc mâm, le lait
de coco et le sucre ; faites revenir jusqu’à ce que les coques s’ouvrent ; jetez celles qui
sont encore fermées.  
3. Ajoutez le choy sum ; faites revenir en remuant afin que les feuilles soient fondantes.
Servezparsemé de coriandre fraîche.  