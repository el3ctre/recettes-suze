---
date: 2019-01-01T01:00:00-01:00
title: "SAINT JACQUES, EFFILOCHEE D’ENDIVES A L’ORANGE"
author: Suze
tags: ["entrées"]
---

# SAINT JACQUES, EFFILOCHEE D’ENDIVES A L’ORANGE

Préparation : 15 min  
Cuisson : 10 min  
Pour 2 personnes :  
6 Saint-Jacques, 2 endives  
180 g de beurre, le jus d’1/2 citron,  
le jus d’une orange, le zeste d’1/2 orange,  
sel, poivre du moulin,  
une dizaine de feuilles de cerfeuil  

Creusez la base des endives afin d’en retirer le cône amer, avant de les
couper en 2, puis de les détailler en fines lanières. Arrosez-les de jus de citron.
Faites les cuire 5 min à la poêle dans 30 g de beurre chaud, en les parsemant
de sucre et de sel en cours de cuisson. Mélangez souvent.  
Retirez le corail des Saint-Jacques, coupez les en 3 dans le sens de
l’épaisseur. Versez le jus d’orange dans une casserole. Amenez-le à ébullition
puis ajoutez les 150 g de beurre restant en noisettes, sans cesser de fouetter
l’ensemble. Râpez le zeste de l’orange au dessus de la sauce. Réservez la
sauce au chaud dans un bain-marie. Faites cuire les noix et le corail 30 s de
chaque côté dans une poêle anti-adhésive sans ajout de matière grasse.  
Disposez-les dans des assiettes individuelles, répartissez les endives tout
autour. Arrosez les noix de sauce à l’orange et décorez de cerfeuil.  