---
date: 2019-01-01T01:00:00-01:00
title: "GOUGERE BOURGUIGNONNE"
author: Suze
tags: ["entrées"]
---

# GOUGERE BOURGUIGNONNE

Temps de préparation et de cuisson : 1 h 20  
Pour 6, il faut :  
1/4 de litre d’eau  
80 g de beurre ou de margarine  
125 g de farine  
4 oeufs  
1 pincée de sel, poivre  
150 g de gruyère râpé  

1. Pâte à choux : dans une casserole, faites chauffer l’eau, le sel, la
matière grasse coupée en morceaux. Dès qu’elle est fondue, baissez le feu au
maximum et versez la farine d’un seul coup. Mélangez énergiquement avec
une cuillère en bois jusqu’à ce que la pâte n’adhère plus à la casserole ni à la
cuillère. Hors du feu ajoutez les oeufs un à un en battant bien la pâte avec la
cuillère en bois. Incorporez le gruyère râpé.  
2. Préparez le four thermostat chaud.  
3. Graissez légèrement la plaque à pâtisserie. A l’aide de 2 cuillères à
café, disposez la pâte en petits tas. Enfournez dès que le four est chaud.
Les gougères sont cuites quand elles sont bien dorées.  