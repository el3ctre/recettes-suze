---
date: 2019-01-01T01:00:00-01:00
title: "QUEUES DE LANGOUSTE / LA CUISSON JUSTE"
author: PICARD
tags: ["entrées"]
---

# QUEUES DE LANGOUSTE / LA CUISSON JUSTE
## RECETTE PICARD

* Pour médaillons froids  
déglacez les queues sous l’eau et plongez-les dans l’eau frémissante  
(1/2 l d’eau pour 2 pièces) avec 2 tablettes de court bouillon, sel, poivre en
grains et le jus d’un demi citron. Attendez la reprise de l’ébullition pour
décompter le temps de cuisson.  
- 12 min pour des pièces de 300 g  
- 14 min pour des pièces de 400 g  
- 16 min pour des pièces de 500 g  
Au bout de ce temps, sortez les queues du court-bouillon et placez-les
dans un récipient fermé jusqu’à refroidissement.  
* Pour demies langoustes grillées :  
Faites pocher les queues comme ci-contre mais en réduisant le temps
de cuisson de moitié. Sortez-les du court bouillon et à l’aide d’un grand couteau
fendez les en deux parties. Terminez la cuisson sous le gril du four ou à la
poêle, côté chair (avec matière grasse et herbes) le temps restant (exemple
pour une pièce de 300 g, court-bouillon ou gril : 6 min).  
NB Suze : peut être servi avec une sauce chien :  
3 ciboules, 4 échalotes grises, 1 oignon, 1 petit bouquet de persil, 1
piment vert, 3 gousses d’ail, 1 citron, vinaigre, huile, sel et poivre
Hachez tous les aromates très finement. Réunissez tous les ingrédients
dans une jatte. Pressez le citron, filtrez le jus, ajoutez et mélangez. Salez et
poivrez. Ajoutez 1 cuil. à soupe de vinaigre (de vin blanc), puis fouettez en
versant un filet continu 200 ml d’huile.  
Au moment de servir, versez 3 cuil. à soupe d’eau chaude dans la sauce.
Fouettez vivement pour émulsionner. Servez aussitôt  