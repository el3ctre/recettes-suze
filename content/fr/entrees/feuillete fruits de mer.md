---
date: 2019-01-01T01:00:00-01:00
title: "FEUILLETE DE FRUITS DE MER"
author: Suze
tags: ["entrées"]
---

# FEUILLETE DE FRUITS DE MER

Ingrédients :  
2 petites courgettes  
2 carottes  
1 oignon blanc  
1 blanc de poireau  
1 kg de moules  
1 kg de coques  
200 g de grosses crevettes  
200 g de noix de St Jacques ou de pétoncles  
1 dose de safran  
crème fraîche  
1 oeuf  
1 verre de cognac  
600 g de pâte feuilletée (2 paquets)  

1. Faire ouvrir les moules à sec. Les décoquiller. Conserver le jus. Faire ouvrir les coques
à sec. Eplucher les crevettes, conserver les têtes. Couper les Saint-Jacques en morceaux,
les passer rapidement à la poêle.  
2. Eplucher carottes et courgettes. Couper en petits cubes tous les légumes. Mettre une
cuillère d’huile dans une poêle, faire suer à feu doux en remuant pendant 1/2 h. Ajouter le
jus des coquillages, les têtes de crevettes (et éventuellement des étrilles coupés en
morceau). Faire réduire à feu doux pendant 1 h environ. Retirer les étrilles et les têtes.
Mixer les légumes, ajouter le safran, la crème. Faire bouillir. Réserver au chaud.  
3. Etaler la pâte feuilletée. Tailler un cercle de 1 cm d’épaisseur, à 2 cm du bord tracer un
2e cercle avec un couteau, sans appuyer pour faire le couvercle. Dorer à l’oeuf. Faire
cuire à 180°C jusqu’à ce que la croûte soit bien dorée. Laisser refroidir, enlever le
couvercle, réserver. Enlever à l’intérieur du vol au vent la partie non cuite.  
4. Au moment de servir, réchauffer la sauce et la croûte. Remplir la croûte avec la sauce.
Remettre le couvercle, servir chaud.  
