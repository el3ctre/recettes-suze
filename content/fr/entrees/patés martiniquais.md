---
date: 2019-01-01T01:00:00-01:00
lastmod: 2023-01-18T10:59:00-01:00
title: "PATES CHAUDS, PATE CHAUDS..."
author: Suze
tags: ["entrées"]
---

# PATES CHAUDS, PATE CHAUDS...

{{< notice tip >}}
500g de viande fait à peu près 36 patés. 3 pâtes suffisent si on met un coup de rouleau avant de découper les ronds.
{{< /notice >}}

Farce : 750 g composée à parts égales de porc (saucisse), boeuf, veau ou volaille.  
Pâte feuilletée ou brisée : 3 ou 4.  

Faire revenir dans une sauteuse 1 échalote hachée menue, 1 gousse d’ail. Ajouter la
viande hachée, 2 verres à liqueur de cognac, 2 cui. à café de bouillon de veau, du thym et
1/2 cuil. de 4 épices. Faire cuire jusqu’à ce qu’il n’y ait plus de liquide au fond de la
sauteuse. Laisser refroidir. Ajouter 2 oeufs, bien mélanger.  

Découper des cercles dans la pâte. Répartisser des dômes de farce sur la moitié des
disques, l’autre moitié servant à faire les chapeaux. Presser les bords pour souder les
chapeaux et badigeonner les chapeaux avec du jaune d’oeuf dilué avec une cuil. à soupe
d’eau.  

Enfourner dans le four préchauffé à 180°C jusqu’à ce que les pâtés soient bien dorés.
Servir tiède.  

Les pâtés sont très bons le lendemain soit froids, soit tièdes.  
