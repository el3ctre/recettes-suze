---
date: 2019-01-01T01:00:00-01:00
title: "TARTARE DE SAUMON"
author: Suze
tags: ["entrées"]
---

# TARTARE DE SAUMON
## “Camus”

Saumon frais haché  
Câpres hachées  
Cornichons hachés  
échalotes hachées  
Tabasco  
sel, poivre  
fines herbes  
citrons  
crème fleurette  