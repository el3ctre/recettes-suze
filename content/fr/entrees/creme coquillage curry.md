---
date: 2019-01-01T01:00:00-01:00
title: "CREME DE COQUILLAGES AU CURRY"
author: Suze
tags: ["entrées"]
---

# CREME DE COQUILLAGES AU CURRY

Préparation : 1h 30  
Pour 4 personnes :  
250 g de palourdes  
250 g de coques  
250 g de vénus  
250 g de moules grattées  
50 g de beurre  
50 g de beurre  
50 g de farine  
400 ml de crème fraîche  
3 blancs d’oeufs  
100 g d’épinards  
1 bonne pincée de curry  
sel fin, poivre  

Laver à grande eau tous les coquillages, puis les faire ouvrir séparément
avec une goutte d’eau sur feu vif. Ne pas saler. Retirer les coquillages et ôter
leurs coquilles. Filtrer le jus de cuisson.  
Equeuter et laver les épinards. Les étuver avec une noisette de beurre.  
Saler et égoutter et presser légèrement pour ôter l’excédent du jus de cuisson.  
Monter les blancs en neige bien ferme puis incorporer délicatement les
feuilles d’épinards ciselées grossièrement et cuire les blancs dans l’eau
frémissante salée, 5 min de chaque côté après les avoir moulé à l’aide d’une
louche. Egoutter le délicatement sur un linge.  
Confectionner un roux blanc avec le beurre et la farine. Verser dessus 300
ml de bouillon de coquillages puis 400 ml de crème fraîche. Vérifier
l’assaisonnement et ajouter une pincée de curry.  
Répartir les coquillages dans les assiettes creuses, poser un oeuf en
neige au centre, napper de crème au curry.  
Servir aussitôt.  