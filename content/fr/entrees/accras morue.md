---
date: 2019-01-01T01:00:00-01:00
title: "ACRAS DE MORUE"
author: Suze
tags: ["entrées"]
---

# ACRAS DE MORUE

Ingrédients :  
200 g de farine  
150 g de filets de morue salée  
250 ml d’eau  
2 gousses d’ail  
5 oignons pays ou cives ou ciboulettes  
5 branches de persil plat  
1 petit morceau de piment rouge ou une pointe de piment  
à l’huile  
2 branches de thym  
1 cuillère à soupe de bicarbonate  
1 cuillère à soupe de vinaigre  

Faites dessaler 150 g de morue pendant 12 h. Egouttez la morue. Retirer
la peau et les arêtes. Emiettez finement la chair (au mixer).  
Hachez menu les cives, l’ail, le persil. Incorporez ce hachis à la morue.
Pliez jusqu’à consistance homogène. Poivrez.  
Faites une pâte fluide avec 200 de farine et un peu plus de 250 ml d’eau.  
Incorporez la morue et les épices à la pâte. Poivrez. Ajoutez un peu de piment
rouge. Eventuellement ajoutez du sel.  
Ajoutez 1 cuillère à soupe de bicarbonate et 1 cuillère à soupe de vinaigre.  
Remuez et laissez reposer pendant 30 min à 1 h sous un linge.  
Formez les acras avec une petite cuillère et faites-les cuire environ 5 min
dans une friteuse. Egouttez-les sur du papier absorbant.  
Servez chaud.  