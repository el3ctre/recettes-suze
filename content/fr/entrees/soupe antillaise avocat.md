---
date: 2019-01-01T01:00:00-01:00
title: "LA SOUPE ANTILLAISE A L’AVOCAT"
author: Suze
tags: ["entrées"]
---

# LA SOUPE ANTILLAISE A L’AVOCAT

Pour 4 personnes :  
3 avocats mûrs  
1 l de bouillon de boeuf  
1 grosse tomate bien mûre  
1 oignon nouveau  
1 gousse d’ail  
1 petit piment fort  
1 citron vert  
100 g de crème épaisse  
sel, poivre  

Dans le mixer, écraser ensemble le piment, l’oignon, la gousse d’ail
épluchée et la chair des avocats, diluer cette purée avec le bouillon, saler
légèrement, verser dans une casserole. Poser sur le feu et laisser mijoter une
dizaine de minutes.  
Ajouter la chair de la tomate pelée, épépinée et coupée en petits dés puis
la crème fraîche et un peu de poivre. Laisser frémir 3 ou 4 min et servir avec
des quartiers de citron vert.  

Notre astuce : quand il fait chaud, vous pouvez servir cette soupe glacée,
mais sans croûton.  