---
date: 2019-01-01T01:00:00-01:00
title: "SOUPES DE MOULES EN FEUILLETE"
author: Suze
tags: ["entrées"]
---

# SOUPES DE MOULES EN FEUILLETE

Préparation 30 min + attente 1 h  
Cuisson : 40 min + 15 min.  
Pour 4 personnes :  
2 l de moules  
500 g de pâte feuilletée étalée  
1 jaune d’oeuf  
150 g de crème fraîche  
Pour la cuisson et l’assaisonnement :  
2 échalotes  
1 oignon  
2 blancs de poireaux  
2 carottes  
3 tomates  
4 cuillères à café de fumet de crustacés  
200 ml de vin blanc sec  
30 g de beurre  
2 doses de safran, poivre  
Pour le bouquet garni : 10 queues de persil  
1 tige de céleri-branche  
1 brin de thym  

1. Nettoyez les moules à l’eau. Egouttez-les. Grattez-les une par une et
retirez les filaments qui dépassent de la coquille.  
Pelez les échalotes. Emincez-les et placez-les dans une marmite. Ajoutez
les moules et le vin blanc. Faites cuire sur feu vif. Brassez les moules en
secouant la marmite en cours de cuisson.  
Retirez-les au fur et à mesure qu’elles s’ouvrent. Otez les coquilles, 
réservez la chair dans un bol recouvert de film plastique. Réservez le jus filtré.  
2. Pelez l’oignon et coupez-le en tranches. Rincez les blancs de poireaux.  
Emincez-le finement. Epluchez les carottes et détaillez-les en bâtonnets. Pelez,
épépinez les tomates et coupez-les en petits morceaux.  
Dans une casserole, faites revenir l’oignon avec le beurre jusqu’à ce qu’il
devienne translucide. Ajoutez les tomates, les poireaux et les carottes et laissez
revenir et réduire à leur tour 5 min. Délayez le safran dans 2 cuillerées à soupe
d’eau chaude.  
3. Liez la tige de céleri avec les queues de persil et le brin de thym.
Ajoutez-le dans la casserole. Saupoudrez les légumes du fumet de crustacés.
Ajoutez 750 ml d’eau, le liquide de cuisson des moules et le safran. Poivrez.
Portez à ébullition et faites cuire doucement pendant 20 min. Ajoutez la
crème fraîche, laissez refroidir.  
Déroulez la pâte feuilletée. Avec un couteau, découpez 4 disques plus
grands que les bols de service, pour qu’ils dépassent de 3 cm tout autour.  
4. Répartissez la soupe froide et les moules dans les bols.  
Diluez le jaune d’oeuf avec 2 cuillères à soupe d’eau et badigeonnez-en
les bords extérieurs. Recouvrez des disques de pâte et pressez un peu pou les
coller. Dorez au jaune d’oeufs puis tracez sur la pâte des croisillons. Placez les
bols 1 h au réfrigérateur.  
Préchauffez le four à 210°C.  
Posez les bols sur une plaque à pâtisserie. Enfournez.  
 Laissez gonfler et dorer pendant 15 min. Servez.  