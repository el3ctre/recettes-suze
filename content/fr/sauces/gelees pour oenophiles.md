---
date: 2019-01-01T01:00:00-01:00
title: "GELEES POUR OENOPHILES"
author: Suze
tags: ["sauces"]
---

# GELEES POUR OENOPHILES

750 ml de bon vin  
500 ml jus de pomme naturel  
800 g de sucre  

Verser le vin dans la bassine à confitures. Porter à ébullition, puis flamber.  
Ajouter le jus de pomme et le sucre.  
Faire cuire à feu vif pendant 15 minutes  
Ecumer, mettre en pots  

->viandes froides, rôti de porc  