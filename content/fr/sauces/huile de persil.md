---
date: 2019-01-01T01:00:00-01:00
title: "HUILE DE PERSIL"
author: Suze
tags: ["sauces"]
---

# HUILE DE PERSIL
## OU DE CIBOULETTES OU DE ACHE

Trier et laver le persil en éliminant les grosses queues. Mixer le persil avec
l’huile et une pointe de sel. Filtrer à travers une passoire. Laisser
macérer 4 h et mettre en flacons  

150 g de persil /250 ml d’huile de pépin de raisins  
50 g de ciboulette ou de ache /80 ml d’huile  