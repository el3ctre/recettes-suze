---
date: 2019-01-01T01:00:00-01:00
title: "COMPOTE D’OIGNONS"
author: Nicole
tags: ["sauces"]
---

# COMPOTE D’OIGNONS
## recette Nicole

1- 700 g d’oignons  
2- 120 g de beurre  
3 - 160 g de sucre en poudre  
4 - 100 ml de vinaigre  
5 - 250 ml de vin d’Algérie ou d’un vin corsé  

Mélanger : 1 + 2 + 3 + sel et poivre  
cuisson : 1/2 h  
Ajouter : 4 + 5  
+ 30 ml de grenadine (ou quelque chose de rouge : cerise....)  
Cuisson 1/2 h à feu doux  
On peut ajouter : raisins secs, abricots secs, pruneaux.....  