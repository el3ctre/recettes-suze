---
date: 2019-01-01T01:00:00-01:00
title: "CAKE AUX HERBES"
author: Suze
tags: ["plats"]
---

# CAKE AUX HERBES

Ingrédients :  
200 g de farine  
3 oeufs  
150 ml de lait chaud  
50 ml d’huile  
100 g de jambon blanc  
60 g de bacon  
1 sachet de levure  
ciboulette, persil, menthe, coriandre, cerfeuil, estragon... à volonté  
sel, poivre  

Mélanger au robot : farine, huile, oeufs, 1/2 cuil. à café de sel, poivre jusqu’à ce que le
mélange soit lisse.  
Verser ensuite le lait chaud. Faire tourner 1 min de plus.  
Ajouter ensuite les herbes, le jambon, le bacon coupé en dés.  
Verser la préparation dans un moule à cake beurré.  
Faire cuire 1 heure à 180 °C (au moins).  