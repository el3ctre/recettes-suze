---
date: 2019-01-01T01:00:00-01:00
title: "QUICHE PROVENCALE"
author: Suze
tags: ["plats"]
---

# QUICHE PROVENCALE

Préparation : 25 min  
Cuisson : 50 min  

Pour 6 personnes :   
100 g de champignons de Paris  
10 tomates cerises  
10 oignons blancs nouveaux  
15 olives noires dénoyautées  
300 g de pâte brisée  
3 brins de basilic  
3 œufs  
20 cl crème liquide  
30 cl lait entier  
1 c. à s. huile d’olive  
30 g beurre  
Poivre, muscade râpée, sel  

Beurrez le moule à tarte. Etalez la pâte, tapissez-en le moule et mettez-le au réfrigérateur.  
Nettoyez les oignons et coupez-les en deux. Coupez la base du pied des champignons.  
Lavez-les rapidement, séchez-les et coupez-les en quatre. Lavez les tomates cerises,
ciselez le basilic. Chauffez le four th 7 (210° C).  
Chauffez l’huile et le reste de beurre dans une poêle. Faites-y sauter les champignons
3 min à feu vif. Ajoutez les oignons et laissez cuire encore 2 min. Salez et poivrez.  
Mélangez les œufs au fouet dans une jatte avec le lait, la crème et le basilic ciselé.  
Ajoutez sel, poivre et muscade râpée.  
Disposez les légumes et les olives dans la tarte, versez la crème dessus.  
Cuisez au four 45 min, la crème doit être prise. Servez chaud ou tiède.  