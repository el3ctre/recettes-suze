---
date: 2019-01-01T01:00:00-01:00
title: "TARTE A L’AUBERGINE, AU FROMAGE DE BREBIS"
author: Suze
tags: ["plats"]
---

# TARTE A L’AUBERGINE, AU FROMAGE DE BREBIS ET A LA FLEUR DE THYM

Préparation : 30 min  
Cuisson : 30 min  
Repos : 1 h  

Pour 4 personnes :  
250 g de farine  
10 g de levure de boulanger  
1 cuil. à café de sucre  
1/2 cuil. à café de sel fin  
150 ml d’eau tiède  
100 ml d’huile d’olive + 2 cuil. à soupe  
2 aubergines, 4 tomates  
2 oignons, 2 gousses d’ail  
1 cuil. à café de concentré de tomate  
1 cuil. à soupe d’herbes de Provence   
1 cuil. à soupe de fleur de thym  
100 ml de bouillon de viande  
100 g de fromage frais de brebis (type brocchio ou féta)  
sel, poivre  

Emiettez et délayez la levure avec la sucre et 50 ml d’eau tiède. Laissez
reposer 15 min, jusqu’à ce que des bulles se forment. Versez la farine dans un
saladier, creusez un puits. Versez la levure, le reste d’eau, le sel et 50 ml
d’huile. Pétrissez pour obtenir une pâte lisse et élastique, qui se détache
facilement du plan de travail. Formez une boule, couvrez d’un linge, laissez
reposer 45 min au tiède.  
Retirez le pédoncule des aubergines, taillez-les en très petits dés.  
Epépinez les tomates et concassez-les. Epluchez l’ail et l’oignon et émincez-les
finement. Diluez le concentré de tomate dans un bouillon.  
Faites revenir les oignons avec le reste d’huile dans une sauteuse sur feu
vif. Ajoutez ail, tomates, herbes, aubergines, sel, poivre. Dès que les
aubergines ont absorbé l’huile, versez le bouillon. Faites cuire 10 min à feu vif.  
Laissez tiédir.  
Allumez le four à 240°C.  
Aplatissez la pâte en cercle de 1 cm d’épaisseur sur une plaque.  
Façonnez un bourrelet en pinçant le tour. Etalez la préparation d’aubergines.  
Faites cuire 15 min à mi-hauteur du four.  
Morcelez le fromage. Sortez la tarte du four, éparpillez-y le fromage,
parsemez de fleur de thym, arrosez de 2 cuil. d’huile d’olive. Remettez 5 min au
four.  