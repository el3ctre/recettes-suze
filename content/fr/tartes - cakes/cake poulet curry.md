---
date: 2019-01-01T01:00:00-01:00
title: "CAKE AU POULET ET CURRY"
author: Suze
tags: ["plats"]
---

# CAKE AU POULET ET CURRY

Préparation : 25 min  
Cuisson : 55 min 

Pour 6/8 personnes :  
300 g poulet cuit fumé ou nature   
250 g farine  
200 g fromage blanc battu  
70 g raisins secs  
3 œufs  
4 c. à s. huile d’olive  
1 c. à s. curry en poudre  
½ sachet levure chimique  
Sel  

Préchauffez le four th 7 (210° C). Réhydratez les raisins secs 15 min dans un bol d’eau
chaude.  
Pendant ce temps, déballez les blancs, éliminez la peau, essuyez-les avec du papier
absorbant. Coupez-les en cubes. Mélangez la farine et la levure chimique.  
Battez les œufs en omelette, ajoutez le fromage blanc et l’huile en continuant de fouetter.  
Incorporez la farine, curry et ½ à c. de sel, les raisins égouttés et le poulet. Versez dans
un moule à cake 26 cm.  
Enfournez, baissez le th à 6 (180° C) et cuisez 50 à 55 min, le dessus du cake doit être
doré. Vérifiez la cuisson en piquant le milieu avec une pointe de couteau, elle doit ressortir
sèche. Démoulez et servez tiède ou froid.  