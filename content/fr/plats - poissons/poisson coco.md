---
date: 2019-01-01T01:00:00-01:00
title: "POISSON AU COCO"
author: Suze
tags: ["plats"]
---

# POISSON AU COCO

Ingrédients :  
4 tranches de poisson  
jus de citron à volonté, 2 cuillères à soupe d’huile  
1 oignon coupé en fines lamelles  
4 tomates pelées et coupées en cube  
200 ml de jus de coco, coriandre, bois d’Inde, ail pilé, sel poivre, piment  

Assaisonner le poisson avec sel et poivre. Ajouter le jus de citron. Laisser
mariner pendant 15 min.  
Faire frire les oignons sans les faire colorer. Ajouter les tomates et
mélanger, le jus de coco, le poisson, le bois d’Inde, la coriandre, le sel, le
poivre, et du piment.  
Laisser cuire 15 min environ. Servir avec du riz.  