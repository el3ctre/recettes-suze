---
date: 2019-01-01T01:00:00-01:00
title: "GRATIN DE POISSON"
author: Suze
tags: ["plats"]
---

# GRATIN DE POISSON

Préparation : 35 min  
Cuisson : 15 min  
Pour 4 personnes :  
700 g de filets de poisson (cabillaud, lieu)  
200 g de moules  
100 g de crevettes  
50 g d’échalotes hachées  
20 g de beurre  
100 g de champignons de Paris  
300 ml devin blanc  
300 g de crème fraîche épaisse  
150 g d’emmental râpé  
1 court-bouillon  
sel, poivre  

Dans une cocotte, faire ouvrir les moules avec le vin blanc et l’échalote.
Les égoutter. Les laisser refroidir puis les décortiquer. Réserver le jus de
cuisson.  
Plonger les filets de poisson dans le court-bouillon froid. Porter à
ébullition. Eteindre le feu et laisser pocher jusqu’à refroidissement. Les
égoutter. Faire cuire 5 à 10 min à l’étuvée les échantillons préparés et émincés.  
Faire réduire le jus de cuisson des moules. Ajouter la crème. Poursuivre la
réduction à nouveau pour obtenir une sauce onctueuse. Incorporer les
champignons, les moules et las crevettes.  
Dans un plat à gratin disposer les filets de poisson bien égouttés. Napper
de sauce. Saupoudrer d’emmental râpé. Faire cuire 10 à 15 min dans le four
préchauffé à 220°C jusqu’à ce que le plat soit bien gratiné.  
Pour une recette encore plus savoureuse, ajouter sous le poisson un lit
d’épinards frais, cuits quelques minutes dans une poêle.  