---
date: 2019-01-01T01:00:00-01:00
title: "LOTTE AUX SAINT-JACQUES ET AUX OEUFS DE SAUMON"
author: Suze
tags: ["plats"]
---

# LOTTE AUX SAINT-JACQUES ET AUX OEUFS DE SAUMON

Durée de préparation : 20 minutes  
Durée de cuisson : 20 minutes  
Pour 4 personnes :  
1 kg de queue de lotte  
800 g de Saint-Jacques  
60 g de beurre  
100 g d’oeufs de saumon  
300 de poireau émincé  
100 ml de vin blanc sec  
2 cuillères à soupe d’échalotes hachées  
1 cuillère à soupe de persil haché  
sel, poivre  

Retirer l’arête centrale de la lotte, couper la chair en tranches épaisses.
Dans une sauteuse faire chauffer du beurre, y faire revenir les morceaux de
lotte sur les deux faces. Ajouter les échalotes et les poireaux. Laisser cuire
4 minutes. Mouiller avec le vin blanc, saler poivrer et laisser cuire 15 minutes.  
Pendant ce temps, faites revenir les coquilles Saint-Jacques dans une
poêle avec une noix de beurre. Les ajouter à la lotte ainsi que le persil. Goûter
et rectifier l’assaisonnement si nécessaire.  
Dans les assiettes de service, déposer les morceaux de lotte et les noix
de Saint-Jacques. Arroser avec le jus de citron, décorer d’oeufs de saumon.  
Servir aussitôt.  