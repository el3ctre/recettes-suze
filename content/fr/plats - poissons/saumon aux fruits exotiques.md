---
date: 2023-01-13T01:00:00-01:00
title: "Saumon -fruits exotiques"
author: Justine
tags: ["poisson"]
---

# Saumon aux fruits exotiques

Pavé de saumon ou filet de truite  
Mangue  
Ananas  
Fruits de la passion  
Lait  
Sel  
Poivre  
épices ?  

Faire cuire le poisson avec l'ananas et la mangue coupés en petits morceaux.  
Récupérer la pulpe des fruits de la passion et mettre dans une casserole avec du lait.  
Faire chauffer la casserole, y ajouter sel, poivre, épices.  
Ajouter le jus des fruits.  
Réduire un peu la sauce.  

Servir.  
