---
date: 2019-01-01T01:00:00-01:00
title: "LOTTE AU CURRY"
author: Suze
tags: ["plats"]
---

# LOTTE AU CURRY

Pour 6 personnes  
Préparation : 25 min  
Cuisson : 30 min  
1,2 kg de lotte  
1 courgette, 2 carottes  
1 oignon, 1 citron non traité  
100 g de crème fraîche  
1 yaourt  
4 cuillerées de fumet de poisson  
30 g de beurre  
sel, poivre  

Pour l’assaisonnement : 1 cuil à café de curry, 1 dose se safran,
1 point de Cayenne, 1/2 cuil de baies roses  

Pelez la courgette et les carottes. Découpez-les en lanières. Plongez-les 2
min dans l’eau bouillante. Egouttez-les.  
A l’aide d’un couteau économique, prélevez le zeste de citron.  
Ebouillantez-le 5 min. Egouttez-les.  
Coupez la queue de lotte en 6 morceaux. Salez-les.  
Chauffez le beurre dans une cocotte. Faites-y revenir et dorer rapidement
les morceaux de lotte. Retirez-les et égouttez-les.  
Faites ensuite revenir l’oignon, pelé et haché, jusqu’à ce qu’ils deviennent
translucides. Parsemez du fumet de poisson. Versez 200 ml d’eau chaude.  
Laissez bouillir doucement 10 min.  
Délayez le yaourt et la crème dans un peu de cette sauce. Réservez dans
la cocotte. Salez et poivrez. Laissez bouillir et épaissir la crème. Mixez la sauce
et reversez-la dans la cocotte.  
Placez-y les morceaux de lotte. Assaisonnez du curry, du safran, du
Cayenne et des baies roses. Laissez cuire pendant 8 min.  
Ajoutez alors le zeste de citron, les lanières de courgette et de carottes.  
Laissez cuire encore 2 min sur feu vif.  
Accompagnez de tagliatelles ou de riz blanc.  