---
date: 2019-01-01T01:00:00-01:00
title: "PETIT CURRY DES MERS"
author: Suze
tags: ["plats"]
---

# PETIT CURRY DES MERS

Préparation : 30 minutes  
Cuisson : 30 minutes  
Pour 4 personnes :  
12 huîtres  
500 g de moules nettoyées  
8 noix de Saint-Jacques avec leur corail  
1 tomate, 1/4 de poivron vert  
1 pomme, 1 mangue  
1 échalote  
40 g de pignons de pin  
100 ml de vin blanc sec  
100 g de crème liquide, 20 g de beurre  
1 cuil à café de curry doux  
3 cuil à café de fumet de poisson déshydraté  
cerfeuil, poivre blanc  

Taillez la tomate, la mangue et la pomme pelées en petits dés. Découpez
le poivron en minces lanières. Réservez le tout.  
Faites colorer les pignons dans une poêle anti-adhésive. Ouvrez les
huîtres, versez leur eau dans une casserole.  
Placez les moules dans un faitout à feu vif. Décoquillez-les (réservez
quelques une pour la décoration) dès qu’elles sont ouvertes, puis filtrez le jus
de cuisson. Ajoutez-le à l’eau des huîtres avec le vin blanc. Portez à ébullition.
Coupez les noix de Saint-Jacques en deux, puis faites-les pocher avec le
corail dans le bouillon, feu éteint pendant 1 min. Ajoutez les huîtres et les
moules. Laisser pocher encore 1 min avant de les égoutter.  
Pelez et hachez finement l’échalote, faites-la revenir dans le beurre
jusqu’à transparence. Saupoudrez de curry et de fumet de poisson, remuez
quelques secondes avec une cuillère en bois, puis mouillez avec le jus de
cuisson des coquillages. Faites bouillir à feu vif et laissez réduire des 2/3.  
Versez la crème, portez à ébullition, puis retirez du feu. Poivrez.  
Répartissez les coquillages dans des assiettes creuses chauffées. Versez
la sauce par dessus, puis disposez les dés de pommes, de mangue et de
tomate ainsi que les lamelles de poivron. Eparpillez les pignons de pin,
quelques pluches de cerfeuil. Servez sans attendre.  