---
date: 2019-01-01T01:00:00-01:00
title: "MOUSSE DE CABILLAUD ET CREVETTES"
author: Suze
tags: ["plats"]
---

# MOUSSE DE CABILLAUD ET CREVETTES

Pour 5/6 personnes :  
1 filet de cabillaud de 350 g  
100 g de crevettes roses décortiquées  
300g de crème fraîche  
2 jaunes d’oeufs  
beurre  
sel, poivre  
2 tasses de coulis de tomates fraîches  
2 jus de citron  
6 feuilles de basilic  

Hachez les crevettes, la chair de cabillaud  
Faire chauffer le four à 210 °C. Préparer un bain-Marie avec 3 cm d’eau
chaude.  
Au hachis de poisson, ajoutez les jaunes d’oeufs et la crème. Salez et
poivrez.  
Beurrez les moules.  
Versez dans les moules. Couvrez de papier alu. Mettre au four au bainmarie pendant 15 min.
Pendant ce temps préparez le coulis de tomates en ajoutant le citron, le
sel, le poivre t le basilic haché.  
Servir sur assiettes chaudes sur le coulis de tomates.  