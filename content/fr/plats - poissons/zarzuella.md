---
date: 2019-01-01T01:00:00-01:00
title: "ZARZUELLA"
author: Mme Molina
tags: ["plats"]
---

# ZARZUELLA
## (Mme Molina)

Pour 8 personnes :  
3 kg de lotte  
6 beaux calamars  
1,5 kg de moules  
8 écrevisses  
16 gambas  
2 dosettes de safran  
1 verre de cognac  
1 mignonette de vin blanc + 1 verre d’eau  
1 bouquet garni  
15 amandes grillées  
1 oignon, 1 verre d’huile, 2 gousses d’ail  

Piler dans un mortier les gousses d’ail, 15 amandes grillées, le persil, le
cognac. Tourner en mélangeant le tout avec de l’huile comme pour une
mayonnaise. Mettre en attente. Faire ouvrir les moules dans une poêle.  
Faire frire le poisson coupé en tranches farinées. Le ranger dans la paella.
Déglacer. Faire cuire les gambas, les langoustines, les écrevisses, puis
flamber.  
Faire la sauce dans le plat de cuisson : mettre l’ail, le persil, l’oignon, la
tomate hachées, l’eau des moules, le vin blanc, le safran, le basilic le sel, le
poivre et 1 petit morceau de piment.  
Arroser le poisson et donner un tour de bouillon. Versez la sauce pillée, ce
qui donne le velouté. Faire réduire la sauce.  