---
date: 2019-01-01T01:00:00-01:00
title: "Galettes de blé noir"
author: Suze
tags: ["dessert"]
---

# LES CREPES

1. GALETTES DE BLE NOIR  

2 x 25 cl de faine de blé noir  
1/2 25 cl de farine de froment  
eau  
1 oeuf  
1 cuillère à café de miel  
1 cuillère à soupe de gros sel  

Mélangez les deux farines et mettre l’oeuf et le miel au centre  
Versez l’eau petit à petit pour ne pas la noyer (très important).  
Puis battre énergiquement (surtout si on est novice) pendant 10 min.  
Ensuite continuez à verser l’eau pour obtenir une bonne crème lisse.  

La garniture de la crêpe est très variée.  