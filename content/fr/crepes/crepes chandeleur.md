---
date: 2019-01-01T01:00:00-01:00
title: "Crèpes de la chandeleur"
author: Suze
tags: ["dessert"]
---

# LES CREPES

2. CREPES DE LA CHANDELEUR

150 g de farine  
50 g de maïzena  
une pincée de sel  
1/2 sachet de levure chimique alsacienne  
3 cuillerées à soupe d’huile ou de beurre fondu  
400 ml de lait écrémé  
4 oeufs  
4 cuillerées à soupe de rhum  
2 cuillerées à soupe de sucre en poudre  
2 sachets de sucre vanillé  
1 zeste de citron finement haché  

- Dans une terrine, mélangez farine sucre, maïzena, sel, levure, huile ou
beurre, oeufs. Délayez peu à peu avec le lait et le rhum pour obtenir une pâte
bien lisse. ajoutez le zeste de citron.  

- Graissez une poêle à fond épais. Faites-la chauffer, versez une petite
louche de pâte en mince épaisseur. Faites cuire à feu assez vif. Retournez la
crêpe et faites cuire de l’autre côté.  
