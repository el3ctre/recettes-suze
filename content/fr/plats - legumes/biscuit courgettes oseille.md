---
date: 2019-01-01T01:00:00-01:00
title: "BISCUIT DE COURGETTES A L’OSEILLE"
author: Suze
tags: ["plats"]
---

# BISCUIT DE COURGETTES A L’OSEILLE

Préparation : 30 min  
Cuisson : 35 à 40 min  
Pour 4 personnes :  
500 g de courgettes  
1/2 boite d’oseille  
1 cuil. à soupe d’huile d’olive  
4 jaunes d’oeufs  
3 blancs d’oeufs  
50 g de main de mie  
100 ml de lait  
jus de 1/2 citron  
100 g de fromage frais à 20 %  
1 cuil. de lait  
sel, poivre  

Couper les extrémités des courgettes. Les émincer grossièrement sans
les éplucher puis les faire revenir dans l’huile d’olive sans les faire colorer.
Ajouter la moitié” de l’oseille équeutée et lavée. Faire cuire 7 à 8 min. Egoutter
légèrement l’ensemble.  
Ajouter aux légumes mixés la mie de pain trempée dans le lait égouttée,
les jaunes d’oeufs. Saler, poivrer. Monter les blancs en neige. Lorsqu’ils ont
atteint leur volume maximum, les détendre avec le jus de citron, puis les
incorporer délicatement à la préparation.  
Verser l’appareil dans une terrine tapissée de papier sulfurisé. Placer le
moule sur un bain-marie et faire cuire 35 min au four à 180°C. Laisser reposer
le biscuit après cuisson pour que sa texture se mette en place. Démouler sur
une planchette.  
Battre légèrement au fouet le fromage blanc additionné de quelques
gouttes de lait. Couper le biscuit en tranches. Servir avec un cordon de sauce
fromage blanc  