---
date: 2019-01-01T01:00:00-01:00
title: "TATIN DE TOMATES"
author: Suze
tags: ["plats"]
---

# TATIN DE TOMATES AU CHEVRE ET AU THYM

Préparation : 30 min  
Repos : 1 h  
Cuisson : 40 min  

Pour 6 personnes :  
250 g farine   
1 c. à s. pour le plan de travail   
175 g beurre   
1 œuf  
1 kg tomates olivettes  
200 g fromage chèvre mi-sec  
4 brins de thym frais  
50 g cassonnade sucre  
sel, poivre  

Incisez la peau des tomates. Plongez-les 30 secondes dans de l’eau bouillante. Retirezles avec une écumoire, enlevez la peau, puis coupez-les dans la longueur et retirez les
graines. Salez l’intérieur. Retournez-les sur une grille et laissez-les s’égoutter pendant
1 heure.  
Amalgamez du bout des doigts la farine et 125 g de beurre en parcelles jusqu’à obtenir
une masse sableuse. Ajoutez une grosse pincée de sel, œuf et 4 cuil. à s. d’eau. Cessez
de travailler la pâte dès qu’elle est homogène. Emballez-la et laissez la reposer 1 heure.  
Préchauffez le four à th 6/7 (200°C). Etalez la cassonnade dans un moule rond, ajoutez le
reste de beurre en parcelles. Posez sur feu vif et laisser caraméliser en surveillant. Retirez
du feu en inclinant le moule dans tous les sens pour bien répartir le caramel.   
Epongez les tomates avec du papier absorbant. Rangez–les en rosace sur le caramel.  
Répartissez le chèvre en rondelles et la moitié du thym, poivrez. Etalez la pâte sur un plan
fariné, puis déposez-la sur la garniture. Faites glisser le pourtour contre le bord du moule.  

Conseils : Préparez la pâte à tarte dès la veille et gardez-la au frais, elle sera meilleure.  
Choisissez un chèvre pas trop frais, il doit rester en morceaux après la cuisson.  
