---
date: 2019-01-01T01:00:00-01:00
title: "LASAGNES VEGETARIENNES"
author: Suze
tags: ["plats"]
---

# LASAGNES VEGETARIENNES

Pour 4 personnes  
Préparation : 30 min  
Cuisson : 30 min  
Ingrédients :  
8 feuilles de lasagnes sèches  
6 grosses tomates  
1 aubergine  
1 bouquet de basilic  
12 olives noires  
300 g de broccio (ou fromage de chèvre frais ou de la féta)  
60 g de parmesan râpé  
8 cuill. à soupe d’huile d’olive  
farine  
sel, poivre  

-> Ebouillantez les tomates, pelez-les puis coupez-les en tranches. Lavez l’aubergine.
Coupez-la en tranches de 5 mm d’épaisseur. Passez-les dans 2 cuill à soupe de farine et
faites-les rissoler rapidement, des deux côtés, avec 5 cuill. à soupe d’huile d’olive.
Egouttez-les sur du papier absorbant. Salez et laissez-les dégorger.  
-> Faites cuire les lasagnes dans beaucoup d’eau salée, additionnée d’une cuill. d’huile
selon le temps indiqué sur le paquet. Egouttez-les sur un linge.  
-> Allumez le four th. 8 (240 °C). Emiettez le broccio. Posez deux rectangles de pâte côte
à côte dans le fond d’un plat huilé de même dimension. Recouvrez de languettes
d’aubergines, de rondelles de tomates épongées, des deux tiers du broccio, d’olives
noires coupées en deux et de feuilles de basilic. Salez et poivrez à chaque couche.
Continuez le montage en finissant par deux feuilles de lasagnes.  
-> Parsemez-les du broccio restant mélangé au parmesan. Arrosez du reste d’huile.
Enfournez, laissez cuire et dorer 20 min. Servez directement dans le plat de cuisson.  