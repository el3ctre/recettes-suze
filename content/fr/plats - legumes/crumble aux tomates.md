---
date: 2019-01-01T01:00:00-01:00
title: "CRUMBLE AUX TOMATES"
author: Suze
tags: ["plats"]
---

# CRUMBLE AUX TOMATES

Préparation : 20 minutes  
Attente : 30 min  
Cuisson : 30 min  
Pour 4 personnes :  
1,5 kg de grosses tomates  
3 gousses d’ail, 2 oignons  
2 cuil à soupe de persil ciselé  
2 brins de basilic pour la finition  
80 g de pecorino ou de parmesan râpé  
150 g de mie de pain rassise  
4 cuil à soupe d’huile d’olive  
2 cuil à soupe de sucre en poudre  
sel, poivre  

Lavez, séchez et coupez les tomates en tranches épaisses. Saupoudrez
de sel sur les deux faces. Laissez dégorger 30 min sur une grille tapissée de
papier absorbant.  
Allumez le four à 180 °C. Faites revenir 2 gousses d’ail et les oignons
pelés et hachés dans une cuil. à soupe d’huile d’olive jusqu’à ce qu’ils soient
translucides. Laissez refroidir.  
Si nécessaire, faites dessécher le pain au four, puis mouliner sa mie et
mélangez-la avec le fromage râpé, les herbes ciselées, la fondue d’oignons et
d’ail, le sucre, le sel et le poivre.  
Frottez un plat à gratin avec la gousse d’ail restante, huilez-le, ajoutez les
tomates épongées en 1 ou 2 couches superposées et parsemez de panure.
Aspergez de 2 cuil. à soupe d’huile et faites cuire au four pendant 30 min.
Pour servir, parsemez de feuilles de basilic.  
Servez avec un poisson ou des côtelettes de porc grillées.  