---
date: 2019-01-01T01:00:00-01:00
title: "RIZ RUMBA LAYA"
author: Laurent Castanet
tags: ["plats"]
---

# RIZ RUMBA LAYA
## (recette L Castanet)

Ingrédients :  
1 kg de porc (pointe ou échine) ou 60 % porc et 40 % boeuf  
100 g de lardons  
2 oignons  
6 ou 7 poivrons  
2 cuillères à soupe de curry  
500 g de crevettes  
1 banane et/ou raisins secs  
1cube de bouillon  
fruits secs  
sel, poivre  

Faites revenir le porc et les lardons, puis le retirer.  
Faire revenir les oignons.  
Ajoutez le porc, les lardons, les poivrons coupés en lamelles, le curry.  
Ajoutez le cube de bouillon, les fruits secs, le sel, le poivre  
Laissez cuire 3/4 h à 1 h.  
Ajoutez les crevettes décortiquées au dernier moment et vérifier
l’assaisonnement  
Parallèlement faire cuire du riz servi séparément.  