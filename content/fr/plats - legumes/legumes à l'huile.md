---
date: 2019-01-01T01:00:00-01:00
title: "LEGUMES A L’HUILE"
author: Suze
tags: ["plats"]
---

# LEGUMES A L’HUILE

## Aubergines : 

faire bouillir 20 min dans du vin blanc et du vinaigre blanc.  
Ajouter de l’ail haché.  
Essorer sur sopalin pendant une nuit.  
Mettre en bocal :  
1 couche + Thym + huile d’olive + sel + ail  

## Champignons : 

idem mais sans ail

## Tomates séchées : 

faire bouillir 1/4 h dans de l’eau bouillante avec du vinaigre.  
Egoutter  
Mettre en bocal avec câpres, fenouil ou thym (ne pas mélanger thym et
fenouil).  
Recouvrir d’huile  
Attendre 3 semaines.  