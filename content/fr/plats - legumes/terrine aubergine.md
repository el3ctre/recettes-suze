---
date: 2019-01-01T01:00:00-01:00
title: "TERRINE D’AUBERGINE"
author: Suze
tags: ["plats"]
---

# TERRINE D’AUBERGINE

Pour 6 à 8 personnes  
Ingrédients :  
2 kg d’aubergines ou 300 g d’aubergines congelées frites (Picard)  
800 g de tomates  
500 g de poivrons rouges  
200 g de crème fraîche  
400 ml d’huile d’olive (si aubergines fraîches)  
2 citrons verts  
30 g de cumin  
1 fromage de chèvre frais  
4 oeufs  
sel, poivre  

Préparation : 45 min  
Cuisson : 1 h  

Epluchez les aubergines, coupez-les en 6 dans le sens de la longueur. Faites les revenir
en plusieurs fois dans de l’huile d’olive chaude. Egouttez-les au fur et à mesure dans une
passoire à pieds, puis dans du papier absorbant.  
 Faites griller les poivrons dans le four. Pelez-les, ôtez les graines, émincez-le, faites les
cuire à la poêle dans très peu d’huile d’olive. Egouttez-les et passez-le au mixer. Dans un
saladier, mélangez la crème, les oeufs battus en omelette et la purée de poivrons, salez,
poivrez.  
Rangez les aubergines dans un moule à cake huilé en saupoudrant de cumin entre
chaque couche. Versez dessus la crème de poivrons. Faites cuire au bain-marie à four
moyen à 180 °C pendant 1 h  
Pelez et épépinez les tomates, mixez-les pour obtenir un coulis. Ajoutez le jus de citrons
verts, de l’huile d’olive, du sel et du poivre.  
Servez la terrine chaude ou froide accompagnée de lamelles de chèvre frais et de coulis
de tomates.  