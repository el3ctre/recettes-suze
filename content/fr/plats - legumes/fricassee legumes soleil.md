---
date: 2019-01-01T01:00:00-01:00
title: "FRICASSEE DE LEGUMES DU SOLEIL"
author: Suze
tags: ["plats"]
---

# FRICASSEE DE LEGUMES DU SOLEIL A LA BROUSSE ET AUX PIMENTS VERTS

Préparation : 20 min  
Cuisson : 25 min  
Pour 4 personnes :  
8 tomates olivettes  
2 petites aubergines  
4 petites courgettes  
2 oignons, 4 gousses d’ail  
1 poivron rouge + 1 vert  
4 piments verts d’Espagne  
100 g de brousse   
100 ml d’huile d’olive  
1 feuille de laurier, 1 bouquet de persil plat  
sel, poivre  

Lavez les légumes. Coupez les tomates, les courgettes et les aubergines
en tranches. Pelez les oignons, détaillez-les en rouelles. Epluchez et émincez
les gousses d’ail. Retirez le pédoncule, les graines et les cloisons des poivrons
et coupez-les en gros carrés. Eliminez le pédoncule des piments, fendez-les
dans la longueur et grattez l’intérieur avec la lame d’un couteau pour enlever
les graines et les lamelles blanches : taillez-les en manières.  
Chauffez 2 cuillerées d’huile d’olive dans une grande sauteuse sur feu vif
et faites-y revenir les rouelles d’oignons jusqu’à légère coloration. Retirez-les et
remettez un peu d’huile.  
Faites revenir pendant quelques minutes successivement les poivrons
avec les piments, puis les courgettes, les aubergines et enfin les tomates, en
remettant à chaque fois un peu d’huile dans la sauteuse. Sortez les légumes
dès qu’ils colorent.  
Remettez tous les légumes dans la sauteuse en ajoutant l’ail, le thym, le
laurier du sel et du poivre. Baissez le feu, couvrez et poursuivez la cuisson
pendant encore 15 minutes.  
Ciselez finement le persil. Emiettez le fromage. Ajoutez-les dans la
sauteuse à la fin de la cuisson. Laissez le fromage fondre, puis servez.  