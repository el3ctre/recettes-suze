---
date: 2019-01-01T01:00:00-01:00
title: "PILAF D’AUBERGINES"
author: Suze
tags: ["plats"]
---

# PILAF D’AUBERGINES

Préparation : 20 min  
Cuisson : 20 min  
Pour 4 personnes :  
12 petites poivrons longs, doux, rouges et verts  
2 tomates, 2 oignons  
2 gousses d’ail, 1 citron  
1 aubergine  
200 g de riz long  
7 cuil à soupe d’huile d’olive  
2 pincées de safran  
sel, poivre  

Laver et sécher les légumes. Pelez, épépinez et concasser les tomates.
Coupez l’aubergine en morceaux sans la peler et faites la rissoler 5 min à
la poêle, sur feu vif, dans 3 cuil à soupe d’huile. Egouttez sur du papier
absorbant. Réservez.  
Ajoutez 2 cuil. à soupe d’huile dans la poêle? Faites-y revenir 8 à 10 min
les poivrons et les dés de tomates en remuant souvent. Salez, poivrez, ajoutez
un filet de jus de citron, mélangez et garder au chaud sur feu doux.  
Porter à ébullition un volume d’eau égal à deux fois celui du riz. Salez et
ajoutez le safran délayé dans l’eau tiède.  
Faites chauffer 2 cuil. à soupe d’huile dans une sauteuse, puis ajoutez les
oignons et la gousse d’ail pelés et hachés. Laissez dorer légèrement 3 min en
remuant. Versez le riz, continuer à remuer jusqu’à ce qu’il soit translucide, puis
ajoutez l’eau safranée et 5 grains de poivre. Portez à ébullition en remuant,
baissez le feu, couvrez et laissez mijoter 12 à 15 min. Toute l’eau doit être
absorbée. Lorsque le riz est cuit laissez le reposer 5 min couvert.  
Servez dans un grand plat chaud le riz recouvert des légumes  
P.S. : diminuer un peu les quantités de riz et augmentez les quantités de
légumes.  