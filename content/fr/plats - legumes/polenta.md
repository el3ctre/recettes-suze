---
date: 2019-01-01T01:00:00-01:00
title: "LA POLENTA"
author: Suze
tags: ["plats"]
---

# LA POLENTA

Faire bouillir le lait et l’eau. Ajouter 2 bouillons cubes de volaille.  
Ajouter 250 g de Polenta en remuant sans cesse.   
En cours de cuisson, ajouter 2 cuil. à soupe d’huile d’olive et le fromage
râpé, la muscade, le poivre.  
Cuit en 5/10 minutes.  