---
date: 2019-01-01T01:00:00-01:00
title: "CHAMPIGNONS “CASA”"
author: Suze
tags: ["plats"]
---

#CHAMPIGNONS “CASA”

Ingrédients :  
3 kg de champignons  
2 verres de vinaigre  
2 l d’huile d’arachide  
2 têtes d’ail  
4 gros oignons ou 8 échalotes  
poivrer, 1 cuil. à soupe de sel  
thym, laurier  

Laver les champignons. Les mettre dans un récipient et les faire sécher
sur le feu.  
Quand il n’y a plus de liquide, arroser de vinaigre et laisser évaporer.
Faire blondir dans une poêle, les oignons ou échalotes avec l’ail.  
Recouvrir les champignons d’huile, ajouter les oignons, saler poivrer.  
Arrêter le feu dès la reprise de l’ébullition et mettre en bocaux encore
chauds. Il faut que l’huile recouvre les champignons.  