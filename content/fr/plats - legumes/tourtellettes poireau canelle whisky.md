---
date: 2019-01-01T01:00:00-01:00
title: "TOURTELETTES AU POIREAU, A LA CANNELLE ET AU WHISKY"
author: Suze
tags: ["plats"]
---

# TOURTELETTES AU POIREAU, A LA CANNELLE ET AU WHISKY

Préparation : 35 min  
Cuisson : 50 min  
Pour 6 personnes :  
500 g de pâte feuilletée pur beurre  
1 kg de poireaux  
250 ml de lait  
100 ml de crème liquide  
2 jaunes d’oeufs + oeuf entier pour dorer la pâte  
30 g de beurre  
1 cuil. à soupe de Maïzena  
1 cuil. à soupe de whisky  
1/2 cuil. à café de cannelle  
sel, poivre  

Laver, épluchez et émincez les poireaux. Faites-les suer 5 min avec le
beurre et la cannelle, puis versez 200 ml de lait et salez. Couvrez et laissez
cuire 20 min sur feu doux. Ajoutez alors la maïzena délayée dans le restant de
lait et mélangez. Incorporer ensuite les jaunes d’oeufs battus avec la crème et
enfin le whisky. Poivrez, puis laisser refroidir.  
Allumez le four à 240°C.  
Etalez la pâte feuilletée sur 3 mm d’épaisseur. Découpez-y 12 cercles de
12 cm de diamètre. Posez-en 6 sur une plaque à pâtisserie. Répartissez la
préparation aux poireaux sur 6 ronds en laissant 2 cm libres sur les bords ;
humectez ceux-ci au pinceau. Recouvrez avec les autres cercles et pressez le
pourtour pour souder la pâte. Egalisez les bords avec un couteau tranchant et
laissez reposer 30 min au réfrigérateur.  
Dorer ensuite les couvercles à l’oeuf battu et faites cuire les tourtelettes
20 min au centre du four en baissant le four à 210°C au bout de 5 min de
cuisson.  
Sortez les petites tourtes du four lorsqu’elles sont bien dorées. Servez-les
chaudes.  
Vin conseillé : accompagnez d’un riesling d’Alsace à 8°C  